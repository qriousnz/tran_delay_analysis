#! /usr/bin/env python3

from webbrowser import open_new_tab

import folium

import conf
import report_helpers
import seg_helpers

def _add_annotation(map_osm, location, text):
    html_text = '<div style="font-size: 9pt;">{}</div>'.format(text)
    marker_icon = folium.features.DivIcon(
        icon_size=(450, 50),
        icon_anchor=(0, 20),  ## x, y from top left corner.
        html=html_text, )
    folium.map.Marker(  ## https://github.com/python-visualization/folium/issues/340#issuecomment-179673692
        location,
        icon=marker_icon
    ).add_to(map_osm)

def _plot_circle(map_osm, location, radius, colour, popup=None):
    kwargs = {'location': location, 'radius': radius, 'color': colour}
    if popup:
        kwargs['popup'] = popup
    folium.CircleMarker(**kwargs).add_to(map_osm)

def _add_icon_marker(map_osm, location, icon=None, colour=None, popup=None):
    """
    https://github.com/lvoogdt/Leaflet.awesome-markers
    """
    kwargs = {'location': location}
    if icon:
        icon_kwargs = {'icon': icon}
        if colour: icon_kwargs['color'] = colour if colour else 'red'
        kwargs['icon'] = folium.Icon(**icon_kwargs)
    if popup: kwargs['popup'] = popup
    folium.Marker(**kwargs).add_to(map_osm)
    
def _get_path_dets(main_heading, reverse):
    F = 'forwards'
    R = 'reverse'
    LINE_HORIZ = 0.0001
    LINE_LEFT = -LINE_HORIZ
    LINE_RIGHT = +LINE_HORIZ
    LINE_VERT = 0.00005
    LINE_UP = +LINE_VERT
    LINE_DOWN = -LINE_VERT
    TEXT_HORIZ = 15
    TEXT_LEFT = +TEXT_HORIZ
    TEXT_RIGHT = -TEXT_HORIZ
    TEXT_VERT = 30
    TEXT_UP = +TEXT_VERT
    TEXT_DOWN = -TEXT_VERT
    TEXT_X_OFFSET = -10  ## may need to be leftward of polyline which is also leftward of original position. Both text directions can be added to to go up and right
    TEXT_Y_OFFSET = 10
    path2dets = {
        (conf.E, F): {
            'desc': "No line changes, text above, left-aligned",
            'line_x': 0,
            'line_y': 0,
            'text_x': 0,
            'text_y': TEXT_UP,
            },
        (conf.W, F): {
            'desc': "No line changes, text below, right-aligned",
            'line_x': 0,
            'line_y': 0,
            'text_x': 0,
            'text_y': TEXT_DOWN,
            'text_align': conf.RIGHT_ALIGN,
            'arrow_left': True,
            },
        (conf.N, F): {
            'desc': "No line changes, text left, right-aligned",
            'line_x': 0,
            'line_y': 0,
            'text_x': TEXT_LEFT,
            'text_y': 0,
            'text_align': conf.RIGHT_ALIGN,
            'arrow_left': False,
            },
        (conf.S, F): {
            'desc': "No line changes, text right, left-aligned",
            'line_x': 0,
            'line_y': 0,
            'text_x': TEXT_RIGHT,
            'text_y': 0,
            'text_align': conf.LEFT_ALIGN,
            'arrow_left': True,
            },
        (conf.E, R): {
            'desc': "Line above, text extra above, left-aligned",
            'line_x': 0,
            'line_y': LINE_UP,
            'text_x': 0,
            'text_y': TEXT_UP + TEXT_Y_OFFSET,
            'text_align': conf.LEFT_ALIGN,
            'arrow_left': False,
            },
        (conf.W, R): {
            'desc': "Line below, text extra below, right-aligned",
            'line_x': 0,
            'line_y': LINE_DOWN,
            'text_x': 0,
            'text_y': TEXT_DOWN + TEXT_Y_OFFSET,
            },
        (conf.N, R): {
            'desc': "Line left, text extra left, right-aligned",
            'line_x': LINE_LEFT,
            'line_y': 0,
            'text_x': TEXT_LEFT + TEXT_X_OFFSET,
            'text_y': 0,
            },
        (conf.S, R): {
            'desc': "Line right, text extra right, left-aligned",
            'line_x': LINE_RIGHT,
            'line_y': 0,
            'text_x': TEXT_RIGHT + TEXT_X_OFFSET,
            'text_y': 0,
            },
    }
    main_heading2dets = {
        conf.E: {'text_align': conf.LEFT_ALIGN, 'arrow_left': False},
        conf.W: {'text_align': conf.RIGHT_ALIGN, 'arrow_left': True},
        conf.N: {'text_align': conf.RIGHT_ALIGN, 'arrow_left': False},
        conf.S: {'text_align': conf.LEFT_ALIGN, 'arrow_left': True},
    }
    direction = R if reverse else F
    path_key = (main_heading, direction)
    path_dets = path2dets[path_key]
    main_heading_dets = main_heading2dets[main_heading]
    path_dets.update(main_heading_dets)
    return path_dets

def _uuid_dets2map(details, map_osm, main_heading, reverse=False):
    """
    Add a specific segment to the map with a coloured polyline and any
    annotations.
    """
    color = report_helpers.reverse2color(reverse)
    path_dets = _get_path_dets(main_heading, reverse)
    line_x = path_dets['line_x']
    line_y = path_dets['line_y']
    text_x = path_dets['text_x']
    text_y = path_dets['text_y']
    text_align = path_dets['text_align']
    ## polyline
    origin_coords = details['origin_coords']
    dest_coords = details['dest_coords']
    coords = details['coords']
    if reverse:
        reverse_origin_coords = (origin_coords[0]+line_y,
            origin_coords[1]+line_x)
        reverse_dest_coords = (dest_coords[0]+line_y,
            dest_coords[1]+line_x)
        coords = (reverse_origin_coords, reverse_dest_coords)
    polyline = folium.PolyLine(coords, color=color, weight=5)
    map_osm.add_children(polyline)
    ## annotation
    box_width = 100
    heading = details['heading']
    arrow = report_helpers.HEADING2ARROW[heading]
    marker_tpl = ("{arrow} {heading}{seq}" if path_dets['arrow_left']
        else "{heading}{seq} {arrow}")
    marker = marker_tpl.format(arrow=arrow, heading=heading,
        seq=details['sequence_id'])
    anchor_x = text_x if text_align == conf.LEFT_ALIGN else text_x + box_width
    mid_coords = seg_helpers.get_central_coord(details)
    html_text = ('<div style="font-size: 10pt; text-align: {text_align}; '
        'color: {color}">{marker}</div>'.format(text_align=text_align,
        marker=marker, color=color))
    marker_icon = folium.features.DivIcon(
        icon_size=(box_width, 50),
        icon_anchor=(anchor_x, text_y),  ## x, y from top left corner.
        html=html_text, )
    folium.map.Marker(  ## https://github.com/python-visualization/folium/issues/340#issuecomment-179673692
        mid_coords,
        icon=marker_icon
    ).add_to(map_osm)

def basic_map_segs(cur, uuids):
    """
    uuid_dets -- dict, keys inc origin_coords, dest_coords
    """
    debug = False
    n_uuids = len(uuids)
    mid_idx = round(n_uuids/2) - 1  ## e.g. 5 -> 2, 6 -> 2, 7 -> 3
    uuids_clause = seg_helpers.uuids2clause(uuids)
    sql_dets = """\
    SELECT *
    FROM {segdets}
    WHERE {uuids_clause}
    ORDER BY sequence_id
    """.format(segdets=conf.SEGDETS_TBL, uuids_clause=uuids_clause)
    cur.execute(sql_dets)
    segs_dets = cur.fetchall()
    mid_uuid_row = segs_dets[mid_idx]
    origin_coords = seg_helpers.coord_from_str(
        coord_str=mid_uuid_row['origin_coords'])
    dest_coords = seg_helpers.coord_from_str(
        coord_str=mid_uuid_row['dest_coords'])
    location_coord = list(seg_helpers.coords2central_coord(*origin_coords,
        *dest_coords))
    map_osm = folium.Map(location=location_coord, # tiles='Mapbox Bright',
        zoom_start=16)
    for seg_dets in segs_dets:
        ## line
        origin_coords = seg_helpers.coord_from_str(
            coord_str=seg_dets['origin_coords'])
        dest_coords = seg_helpers.coord_from_str(
            coord_str=seg_dets['dest_coords'])
        coords = [origin_coords, dest_coords] 
        polyline = folium.PolyLine(coords, color='blue', weight=5)
        map_osm.add_children(polyline)
        ## annotation
        seg_heading = seg_helpers.get_seg_heading(seg_dets['heading'],
            seg_dets['sequence_id'])
        if debug: print(seg_heading)
        central_coord = seg_helpers.coords2central_coord(*origin_coords,
            *dest_coords)
        _add_annotation(map_osm, location=central_coord, text=seg_heading)
        ## popup
        popup = "{} (uuid '{}')".format(seg_heading, seg_dets['uuid'])
        _add_icon_marker(map_osm, location=central_coord, icon=None, #'info-sign',
            colour='blue', popup=popup)
    fpath = ("/home/gps/Documents/transport/delay_analysis/reports/"
        "segments_mapped.html")
    url = "file://{}".format(fpath)
    map_osm.save(fpath)
    open_new_tab(url) 

def map_segs(cur, report_id, inc_uuid, uuid_dets, reverse_uuid_dets=None,
        display=True):
    """
    https://www.google.co.nz/maps/dir/-37.527860000000004,+175.15948/-37.528330000000004,+175.15937000000002/@-37.5280907,175.1572367,17z/data=!3m1!4b1!4m9!4m8!1m3!2m2!1d175.15948!2d-37.52786!1m3!2m2!1d175.15937!2d-37.52833"
    """
    inc_uuid_dets = seg_helpers.get_inc_uuid_dets(inc_uuid, uuid_dets)
    if reverse_uuid_dets is None:
        reverse_uuid_dets = []
    incident_coord = seg_helpers.get_central_coord(inc_uuid_dets)
    main_heading = seg_helpers.get_main_heading(uuid_dets)
    zoom_start = 15 if main_heading in [conf.N, conf.S] else 17  ## assuming wide and short image
    map_osm = folium.Map(location=list(incident_coord), zoom_start=zoom_start)
    reverse_main_heading = seg_helpers.get_main_heading(reverse_uuid_dets)
    for details in uuid_dets:
        _uuid_dets2map(details, map_osm, main_heading, reverse=False)
    for reverse_details in reverse_uuid_dets:
        _uuid_dets2map(reverse_details, map_osm, reverse_main_heading,
            reverse=True)
    folium.CircleMarker(incident_coord, 15, 'red').add_to(map_osm)
    fpath = ("/home/gps/Documents/transport/delay_analysis/reports/"
        "incident_segments_{}.html".format(report_id))
    url = "file://{}".format(fpath)
    map_osm.save(fpath)
    if display:
        open_new_tab(url)
    return url
