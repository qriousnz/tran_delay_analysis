#! /usr/bin/env python3

from datetime import timedelta

import conf
import date_helpers
import report_helpers
import seg_helpers

import matplotlib.dates as mdates
import matplotlib.pyplot as plt

IMG_TPL = ("<td colspan={colspan}>"
    "\n  <img class='{img_class}' src='{chart_path}'>\n  "
    "<p class='shrink'>{audit_sql}</p>\n</td>")

AUDIT_SQL_TPL = """\
    SELECT round(adjusted_delay_pct) AS increase, *
    FROM {traffic_incidents}
    WHERE uuid = '{{uuid}}' AND {{clause}}
    ORDER BY adjusted_delay_pct DESC
    <br><br>
    SELECT uuid, date, duration, duration_in_traffic
    FROM {segment_traffic}
    WHERE uuid = '{{uuid}}' AND {{clause}}
    """.format(traffic_incidents=conf.TRAFFIC_FINAL_TBL,
        segment_traffic=conf.SEGMENT_TRAFFIC_TBL)

def set_hour_ticks(ax):
    major_locator = mdates.HourLocator()
    ax.xaxis.set_major_locator(major_locator)
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%-I%p\n%a-%d'))
    minor_locator = mdates.MinuteLocator(byminute=(15, 30, 45))
    ax.xaxis.set_minor_locator(minor_locator)
    ax.xaxis.set_minor_formatter(mdates.DateFormatter('%M'))
    plt.tick_params(axis='x', which='minor', labelsize=6)

def make_increase_mpl_img(data, focus_dt, ymin, ymax, seg_heading_color,
        title_seg_heading, title_direction, img_name, width=12, height=2.5,
        axis_times=conf.AXIS_HOUR, include_inc_zone=True,
        include_inc_lbls=True, is_inc=True):
    """
    Display provenance (source level) so we can place most weight on
    directly-polled.

    WARNING - Because the data is time-zoned it is important to set timezone
    setting in matplotlibrc to e.g. Pacific/Auckland (if that is what the data
    refers to).

    As per http://matplotlib.org/1.3.1/users/customizing.html
    >>> import matplotlib
    >>> matplotlib.matplotlib_fname()
    '/usr/local/lib/python3.5/dist-packages/matplotlib/mpl-data/matplotlibrc'
    """
    fig = plt.figure(figsize=(width, height))
    ax = fig.add_subplot(111)
    x = [data_dic['date'] for data_dic in data]
    
    ## delay_pct - difference of actual duration from the typical
    ## duration google supplies (avg across all times of day etc).
    ## Should vary across day and be +ve during rush hour and negative in
    ## middle of night.

    ## y_adjusted_delay_pct - % difference from expected duration
    ##
    ## Because the avg diff changes over day e.g. peak hour will be higher
    ## than middle of night, we get step changes in the lines for this value
    ## cf raw duration diff. Should dampen down extremes.
    #y_delay_pct = [data_dic['delay_pct']
    #    for data_dic in data]
    y_0 = []
    y_1 = []
    y_2 = []
    for data_dic in data:
        y_val = data_dic['adjusted_delay_pct']
        if data_dic['src_level'] == 0:
            y_0.append(y_val)
            y_1.append(None)
            y_2.append(None)
        elif data_dic['src_level'] == 1:
            y_1.append(y_val)
            y_0.append(None)
            y_2.append(None)
        elif data_dic['src_level'] == 2:
            y_2.append(y_val)
            y_0.append(None)
            y_1.append(None)
    ax.plot(x, y_0, color=conf.LIGHT_GREY)
    ax.plot(x, y_1, color=conf.DARK_GREY)
    ax.plot(x, y_2, color=conf.RED)
    if axis_times == conf.AXIS_HOUR:
        set_hour_ticks(ax)
    elif axis_times == conf.AXIS_DAYS:
        major_locator = mdates.DayLocator()
        ax.xaxis.set_major_locator(major_locator)
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%d/%m'))
        minor_locator = mdates.HourLocator(byhour=(6, 12, 18))
        ax.xaxis.set_minor_locator(minor_locator)
        ax.xaxis.set_minor_formatter(mdates.DateFormatter('%-I%p'))
        plt.tick_params(axis='x', which='minor', labelsize=5)
    elif axis_times == conf.AXIS_MONTHS:
        major_locator = mdates.WeekdayLocator(byweekday=mdates.MO)
        ax.xaxis.set_major_locator(major_locator)
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%a %d/%m'))
    else:
        raise Exception("Unexpected axis_times - {}".format(axis_times))
    fig.text(0.15, 0.70, title_seg_heading,  ## http://matplotlib.org/users/text_props.html
        fontdict={'fontsize': 25, 'fontweight': 'bold',
            'color': seg_heading_color})
    fig.text(0.15, 0.40, title_direction, fontdict={'fontsize': 20,
        'fontweight': 'bold', 'color': conf.DARK_GREY})
    ax.set_ylim(ymin, ymax)
    ax.set_xlabel('Date', fontsize=12)
    plt.xticks(fontsize=8)
    ax.set_ylabel('Delay Increases', fontsize=12)
    ## add focus date line
    if include_inc_zone:
        plt.plot((focus_dt, focus_dt), (ymin, ymax), 'r--', linewidth=2)
        if include_inc_lbls:
            lbl = 'Incident\nstarts' if is_inc else 'Focus date'
            ax.text(focus_dt-timedelta(minutes=12), ymax, lbl, ha='left',
                va='top', fontsize=6, rotation=90)
    ## wrapping up
    img_rel_path = "images/charts/{}".format(img_name)
    img_path = "reports/{}".format(img_rel_path)
    plt.savefig(img_path, bbox_inches='tight')
    #plt.show()
    plt.close(fig)
    print("    Saved '{}'".format(img_path))
    return img_rel_path


## LONG-TERM charts ************************************************************

def make_long_term_chart(cur, inc_uuid, focus_dt, inc_uuid_details, report_id,
        time_series_img_limit, width, height, days_either_side=None,
        include_inc_zone=True, is_inc=True):
    make_img = False
    if time_series_img_limit is None:
        make_img = True
    else:
        if time_series_img_limit >= 1:
            make_img = True
            time_series_img_limit -= 1
    if make_img:
        seg_heading_color = report_helpers.reverse2color(reverse=False)
        if days_either_side is not None:
            dates_clause = date_helpers.get_surrounding_dates_clause(focus_dt,
                hours_either_side=days_either_side*24)
            axis_times = conf.AXIS_DAYS
        else:
            dates_clause = '1=1'
            axis_times = conf.AXIS_MONTHS
        sql_data = """\
        SELECT
        date,
        adjusted_delay_pct,
        src_level
        FROM {traffic_incidents}
        WHERE uuid = %s
        AND {dates_clause}
        ORDER BY date
        """.format(traffic_incidents=conf.TRAFFIC_FINAL_TBL,
            dates_clause=dates_clause)
        cur.execute(sql_data, (inc_uuid, ))
        data = cur.fetchall()
        audit_sql = AUDIT_SQL_TPL.format(uuid=inc_uuid, clause=dates_clause)
        lbl = 'long_term' if days_either_side is not None else 'all_dates'
        img_name = "{}_{}.png".format(lbl, report_id)
        ys = [y['adjusted_delay_pct'] for y in data]
        ymin = min(ys)
        ymax = max(ys)
        seg_heading = seg_helpers.get_seg_heading(
            heading=inc_uuid_details['heading'],
            seq=inc_uuid_details['sequence_id'])
        title_seg_heading = seg_heading
        title_direction = inc_uuid_details['direction']
        chart_path = make_increase_mpl_img(data, focus_dt, ymin, ymax,
            seg_heading_color, title_seg_heading, title_direction, img_name,
            width=25, height=2.5, axis_times=axis_times,
            include_inc_zone=include_inc_zone, include_inc_lbls=False,
            is_inc=is_inc)
        long_term_img = "<tr>{}</tr>".format(IMG_TPL.format(
            chart_path=chart_path, img_class='long-term-image',
            audit_sql=audit_sql, colspan=2))
    else:
        long_term_img = ''
    return long_term_img, time_series_img_limit


## Standard SEG CHARTS *********************************************************

def make_std_seg_chart(cur, dates_clause, focus_dt, ymin, ymax, uuid_details,
        report_id, reverse=False, is_inc=True):
    seg_heading_color = report_helpers.reverse2color(reverse)
    ## seg details
    ## traffic data for seg
    sql_data = """\
    SELECT
    date,
    adjusted_delay_pct,
    src_level
    FROM {traffic_incidents}
    WHERE uuid = %s
    AND {dates_clause}
    ORDER BY date
    """.format(traffic_incidents=conf.TRAFFIC_FINAL_TBL,
        dates_clause=dates_clause)
    cur.execute(sql_data, (uuid_details['uuid'], ))
    data = cur.fetchall()
    ## MPL charting
    seg_heading = seg_helpers.get_seg_heading(heading=uuid_details['heading'],
        seq=uuid_details['sequence_id'])
    title_seg_heading = seg_heading
    title_direction = uuid_details['direction']
    img_name = "{}_{}.png".format(seg_heading, report_id)
    chart_path = make_increase_mpl_img(data, focus_dt, ymin, ymax,
        seg_heading_color, title_seg_heading, title_direction, img_name,
        is_inc=is_inc)
    return chart_path

def make_std_seg_charts(cur, report_id, inc_uuid, focus_dt, dates_clause,
        uuid_dets, reverse_uuid_dets, ymin, ymax, time_series_img_limit,
        is_inc):
    print("Making charts ...")
    images_made = 0
    subreport_dets = [
        (uuid_dets, False),
        (reverse_uuid_dets, True),
    ]
    img_cols = []
    for seg_uuid_dets, reverse in subreport_dets:
        img_col = []
        for uuid_details in seg_uuid_dets:
            if (time_series_img_limit is not None
                    and images_made >= time_series_img_limit):
                break
            audit_sql = AUDIT_SQL_TPL.format(uuid=uuid_details['uuid'],
                clause=dates_clause)
            chart_path = make_std_seg_chart(cur, dates_clause, focus_dt, ymin,
                ymax, uuid_details, report_id, reverse=reverse, is_inc=is_inc)
            img_col.append(IMG_TPL.format(chart_path=chart_path,
                img_class='seg-image', audit_sql=audit_sql, colspan=1))
            images_made += 1
        img_cols.append(img_col)
    return img_cols


## SNAPSHOTS *******************************************************************

def _uuid2color(uuid, src_level, inc_uuid):
    """Mark by whether directly polled or not and whether the incident"""
    if src_level == conf.TERMINAL_LEVEL:
        if uuid == inc_uuid:
            color = 'red'
        else:
            color = '#ff8c8c'  ## pale red
    else:
        if uuid == inc_uuid:
            color = '#3d3939'  ## dark grey
        else:
            color = '#b0a4a4'  ## light grey
    return color

def make_mpl_snapshot_img(data, inc_uuid, ymin, ymax, title, img_name,
        debug=False):
    fig = plt.figure(figsize=(12, 2.5))
    ax = fig.add_subplot(111)
    x = list(range(len(data)))
    x_labels = [seg_helpers.get_seg_heading(data_dic['heading'], data_dic['seq'])
        for data_dic in data]
    y = [data_dic['increase'] for data_dic in data]
    color = [_uuid2color(data_dic['uuid'], data_dic['src_level'], inc_uuid)
        for data_dic in data]
    ax.bar(x, y, 0.8, color=color, edgecolor="none", align='center')
    ax.set_xticks(x)
    ax.set_xticklabels(x_labels)
    ax.axhline(0, color='black')
    ax.set_title(title, fontsize=20, fontweight='bold', loc='left',
        verticalalignment='bottom')
    ax.set_ylim(ymin, ymax)
    ax.set_xlabel('Segments', fontsize=12)
    ax.set_ylabel('Delay Increases', fontsize=12)
    ## wrapping up
    img_rel_path = "images/charts/{}".format(img_name)
    img_path = "reports/{}".format(img_rel_path)
    plt.savefig(img_path, bbox_inches='tight')
    #plt.show()
    plt.close(fig)
    if debug: print("    Saved '{}'".format(img_path))
    return img_rel_path

def make_mpl_snapshot_markers_img(report_id, inc_identified_dt, dates2use,
        ymax):
    img_name = "{}_snapshot_markers.png".format(report_id)
    start_dt, end_dt = date_helpers.get_surrounding_dates(inc_identified_dt)
    fig = plt.figure(figsize=(12, 2.5))
    ax = fig.add_subplot(111)
    x = [start_dt, end_dt]
    y = [ymax, ymax]  ##can't cope if all are None
    ax.plot(x, y)
    set_hour_ticks(ax)
    ax.set_ylim(0, ymax)
    ax.set_xlabel('Date', fontsize=12)
    plt.xticks(fontsize=8)
    ax.set_ylabel('Delay Increases', fontsize=12)
    for date2use in dates2use:
        plt.plot((date2use, date2use), (0, ymax), 'k--', linewidth=1)
    ## wrapping up
    img_rel_path = "images/charts/{}".format(img_name)
    img_path = "reports/{}".format(img_rel_path)
    plt.savefig(img_path, bbox_inches='tight')
    #plt.show()
    plt.close(fig)
    print("    Saved '{}'".format(img_path))
    return img_rel_path

def make_seg_snapshot_chart(data, inc_uuid, inc_start_dt, inc_identified_dt,
        report_id, chart_date, ymin, ymax):
    if chart_date == inc_start_dt:
        inc_lbl = " (Incident start?)"
    elif chart_date == inc_identified_dt:
        inc_lbl = " (Incident identified)"
    else:
        inc_lbl = ''
    title = "{}{}".format(chart_date.strftime("%-I:%M:%S%p %a-%d"), inc_lbl)
    img_name = "{report_id} {title}.png".format(report_id=report_id,
        title=title.replace(' ', '_').replace('?', ''))
    chart_path = make_mpl_snapshot_img(data, inc_uuid, ymin, ymax, title,
        img_name)
    return chart_path

def make_seg_snapshot_charts(cur, report_id, inc_uuid, focus_dt, inc_start_dt,
        inc_identified_dt, dates_clause, uuid_dets, seg_series_img_limit, ymax):
    """
    Make charts which show duration diff increases (delays) across the segments
    for a given snapshot.
    
    Find a middle point (or inc_dt if present) and hop outwards from there,
    tightly at first, then a few bigger jumps for context up until the max
    number of datetimes being charted.
    """
    items = []
    if (seg_series_img_limit is not None and seg_series_img_limit == 0):
        return items
    uuids = [x['uuid'] for x in uuid_dets]
    uuids_clause = seg_helpers.uuids2clause(uuids)
    date_offsets = (-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5)
    dates2use = report_helpers.get_snapshot_dates(cur, focus_dt,
        dates_clause, uuids, uuids_clause, date_offsets)
    ## Make chart showing where date snapshots are to enable comparison with other charts
    chart_path = make_mpl_snapshot_markers_img(report_id, focus_dt,
        dates2use, ymax)
    snapshot_img = IMG_TPL.format(chart_path=chart_path,
        img_class='snapshot-image', audit_sql='', colspan=1)
    items.append("<tr>{}</tr>".format(snapshot_img))
    if seg_series_img_limit is not None:
        seg_series_img_limit -= 1
    ## Make segment ripple detection charts
    start_date = min(dates2use)
    end_date = max(dates2use)
    dates_clause = date_helpers.date_range2clause(start_date, end_date,
        is_str=False)
    ymin, ymax = report_helpers._get_ybounds(cur, uuids, dates_clause)
    for charts_made, chart_date in enumerate(dates2use, 1):  ## 1 because already made snapshot marking image if we got this far
        if (seg_series_img_limit is not None
                and charts_made >= seg_series_img_limit):
            break
        snapshot_data = report_helpers.get_snapshot_data(cur, chart_date,
            uuids_clause)
        chart_path = make_seg_snapshot_chart(snapshot_data, inc_uuid,
            inc_start_dt, inc_identified_dt, report_id, chart_date, ymin, ymax)
        snapshot_img = IMG_TPL.format(chart_path=chart_path,
            img_class='snapshot-image', audit_sql='', colspan=1)
        items.append("<tr>{}</tr>".format(snapshot_img))
    return items
