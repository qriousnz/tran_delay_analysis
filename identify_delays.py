#! /usr/bin/env python3

"""
Identify anomalous congestion visually.
"""
import datetime
import getpass
from pprint import pprint as pp
from pprint import pformat as pf
import statistics

import psycopg2 as pg
import psycopg2.extras as pg_extras
from pytz import timezone

import conf
import date_helpers
import make_src_data
import not4git
import reports
import report_maps
import seg_helpers

nz = timezone('Pacific/Auckland')
MAX_REPORTS = 10  ## doesn't affect special reports, only auto-generated ones
TIME_SERIES_IMG_LIMIT = None ## None = unlimited, 0 for no charts etc
SEG_SERIES_IMG_LIMIT = 0

def _tidy_points_display(points_breakdown):
    points_display = ["{:<30}{:>23}{:>10,}".format(lbl, raw_score, points)
        for lbl, raw_score, points in points_breakdown]
    return points_display

def _get_decongestion_dets(next_uuid, segs_data):
    """
    Should be positive points if low congestion after the alleged incident and
    negative if high congestion afterwards.

    Note - next_uuid might be None (assuming we're the end of the line)

    Try to get as many as 10 subsequent segments if possible.

    e.g. 2% delay --> -4, 0% delay --> 0, -10 delay --> +20
    """
    uuids2check = []
    while True:
        try:
            seg = segs_data[next_uuid]
        except KeyError:  ## when None
            break
        else:
            uuids2check.append(next_uuid)
            next_uuid = seg['next_uuid']
            if len(uuids2check) >= 10:
                break
    n_following_segs = len(uuids2check)
    delay_pcts = [segs_data[uuid2check]['adjusted_delay_pct']
        for uuid2check in uuids2check]
    try:
        mean_delay_pcts = statistics.mean(delay_pcts)
    except statistics.StatisticsError:
        mean_delay_pcts = 0
    decongestion_points = conf.DECONGESTION_DELAY_PUNISHMENT*mean_delay_pcts
    return decongestion_points, mean_delay_pcts, n_following_segs

def _get_pre_incident_free_flowing_avg(cur, inc_start_dt, uuids_clause):
    start_dt = inc_start_dt - datetime.timedelta(minutes=15)
    dates_clause = date_helpers.date_range2clause(start_dt, inc_start_dt,
        is_str=False)
    sql_data = """\
    SELECT AVG(adjusted_delay_pct) AS
      avg_delay_pct
    FROM {traffic_final}
    WHERE {uuids_clause}
    AND {dates_clause} 
    """.format(traffic_final=conf.TRAFFIC_FINAL_TBL, uuids_clause=uuids_clause,
        dates_clause=dates_clause)
    cur.execute(sql_data)
    avg_delay_pct = cur.fetchone()['avg_delay_pct']
    free_flowing_points = conf.FREE_FLOWING_DELAY_PUNISHMENT*avg_delay_pct
    return free_flowing_points, avg_delay_pct

def _get_avg_monthly_edge(cur2, match_tbl, match_id_name, match_id):
    sql_data = """\
    SELECT avg_monthly_edge_tot
    FROM {match_tbl}
    WHERE {match_id_name} = %s
    """.format(match_tbl=match_tbl, match_id_name=match_id_name)
    cur2.execute(sql_data, (match_id, ))
    row = cur2.fetchone()
    if not row:
        raise Exception("sql: {}\nmatch_id: {}".format(sql_data, match_id))
    avg_monthly_edge_tot = float(row['avg_monthly_edge_tot'])
    avg_monthly_edges = avg_monthly_edge_tot
    avg_monthly_edge_points = (conf.AVG_MONTHLY_EDGES_PUNISHMENT
        *avg_monthly_edge_tot)
    return avg_monthly_edge_points, avg_monthly_edges

def _get_root_avg_monthly_edge(cur2, root_seg_uuid):
    match_tbl = conf.ROOT_SEG_EDGE_RATES_TBL
    match_id_name = 'root_seg_uuid'
    match_id = root_seg_uuid
    return _get_avg_monthly_edge(cur2, match_tbl, match_id_name, match_id)

def _get_seg_avg_monthly_edge(cur2, uuid):
    match_tbl = conf.SEG_EDGE_RATES_TBL
    match_id_name = 'uuid'
    match_id = uuid
    return _get_avg_monthly_edge(cur2, match_tbl, match_id_name, match_id)

def get_inc_dets(cur2, edge_uuid, root_seg_uuid, edge_dt, debug=False,
        verbose=False):
    """
    Only collect if an edge.

    edge_dt -- the date we try to identify an incident from and then calculate
    actual incident start and stability.

    * EDGE LOCATION STABILITY
        - data from preceding data for that segment. Has the edge
          been stable?
        - in seg_data for segment see if is_edge
                      |
                  ----x----
                  ----x----
                  ----x----
                      |       :-)  if it flicks around it isn't a geographically
                                   defined incident - might be road works etc

        Must have *some* stability to be considered an incident otherwise just
        moving traffic congestion

    * DELAY DROP around incident (must already be >= EDGE_DROP_PCT_THRESHOLD)
        - data from its matching record in factors (same root uuid
          and date):
        - edge strength (the bigger the delay drop pct, the more likely to be an
          incident)
                        ______
        |  |                   ^
        |  |                   |  :-)  enough change to care about
        |  |  |         ______ v
        |  |  |  |  |

    * QUEUE LENGTH
        - whether the end of a queue, and how long (long queue makes it
          more likely although some queues will be short if there is a road for
          diversion nearby)
          
        ------------x   :-)  possibly more useful for KPI uses than as an
                             indicator

    * PRECEDING CONGESTION (preceding two must >= EDGE_ABS_PRE_INCREASE
        - adjusted delay pct of preceding segment (the higher the
          more likely to be an incident)
              __________
         |  |           ^
         |  |           |  :-)  enough congestion harm to even care about
         |  |  |        |
         |  |  |  |  |  v

    * FOLLOWING DECONGESTION
        - adjusted delay pct of following segment (skipping the immediately
          following one). The lower the more likely to be an incident.

         |  |              really low :-)  cars are breaking free of congestion
         |  |  |         /
         |  |  |  |  |  /

    * PRE-INCIDENT FREE-FLOWING
        - avg delay_pct across all segments for previous 15 minutes

        ✔ ✕ ✔ ✔ ✔ ✔          ✕ ✕ ✔ ✕ ✕ ✔
        ✔ ✔ ✔ ✕ ✔ ✔  :-)     ✔ ✕ ✕ ✕ ✔ ✕  :-(  congested well before incident so
        ✔ ✔ ✔ ✔ ✔ ✔          ✕ ✕ ✔ ✕ ✕ ✕       incident can't have caused it
                                               (temporal logic 101)

    * EDGE RARITY
      ** data from root segment edge rates table:
        - avg monthly edge total for root segment (the higher the average
          number, the less likely this edge indicates an actual incident)

                       |                   :-(  happens all the time!
              |        |             |
          _|__|__|_|___|____|_||_____|

      ** data from segment edge rates table
        - avg monthly edge total for individual segment (as for root segs)
    """
    sql_factors = """\
    SELECT *
    FROM {factors_root_data}
    WHERE date = %s
    AND root_seg_uuid = %s
    """.format(factors_root_data=conf.FACTORS_ROOT_DATE_TBL)
    ## reduce expensive SQL calls
    segs_data_cache = {}  ## needed because will be hitting a factor record more than once if multiple edges for given root seg uuid and date
    #prev_factor_rows_cache = {}  ## gobbles too much memory so live with occasional duplicated effort
    inc_dets = {
        'edge_uuid': edge_uuid,
        'edge_dt': edge_dt,  ## our starting point
        'inc_start_dt': None,
        'inc_identified_dt': None,
        'points_breakdown': [],
        'incident_points': 0,
    }
    ## EDGE LOCATION STABILITY (a cardinal issue if not stable at all)
    ## Get previous records for root seg uuid by date
    ## May be called more than once if multiple edges in a route because same root_seg_uuid and date.
    ## Some are polled milliseconds apart so need to ignore those that
    ## are not actually far enough before. 100 records should be enough
    ## to get to the beginning of nearly all incidents. Good enough to
    ## get points for really stable incidents


    ## A) EARLY EXITS **********************************************************
    ## if not a potential incident (and potential_incidents_only)

    sql_get_prev = """\
    SELECT *
    FROM {factors_root_date}
    WHERE date < %s
    AND root_seg_uuid = %s
    ORDER BY date DESC
    LIMIT 100
    """.format(factors_root_date=conf.FACTORS_ROOT_DATE_TBL)
    cur2.execute(sql_get_prev, (edge_dt, root_seg_uuid))
    prev_factor_rows = cur2.fetchall()
    ## No previous records
    if len(prev_factor_rows) < 1:
        if debug:
            print("Unable to find even one previous record for same root "
                "seg uuid '{}'".format(edge_uuid))
        return None

    ## edge stability
    inc_dets['inc_identified_dt'] = edge_dt
    inc_start_dt = edge_dt
    for n, prev_factor_row in enumerate(prev_factor_rows, 1):
        is_edge = prev_factor_row[conf.SEGS_DATA_KEY][edge_uuid]['is_edge']
        if is_edge:
            inc_start_dt = prev_factor_row['date']
        else:
            break
    if debug:
        print("{} records checked to calculate stability duration".format(n))
    inc_dets['inc_start_dt'] = inc_start_dt
    stability_duration = edge_dt - inc_start_dt
    stability_duration_secs = stability_duration.total_seconds()
    unstable = (stability_duration_secs < conf.MIN_EDGE_STABILITY)
    if unstable:
        if debug:
            print("Not even stable for {:,} seconds".format(
                conf.MIN_EDGE_STABILITY))
        return None  ## not a stable edge at all then not an incident


    ## B) POINTS ***************************************************************

    if stability_duration_secs == 0:
        stability_points = 0
    elif stability_duration_secs < conf.LL_HIGHER_EDGE_STABILITY:
        stability_points = conf.NORMAL_EDGE_STABILITY_POINTS
    else:
        stability_points = conf.HIGH_EDGE_STABILITY_POINTS
    inc_dets['points_breakdown'].append(
        ("Edge location stability",
         "{:,}s".format(round(stability_duration_secs)),
         stability_points))
    inc_dets['incident_points'] += stability_points
    ## Get factors row for entire route (root seg uuid) containing alleged incident.
    segs_data = segs_data_cache.get((edge_dt, root_seg_uuid))
    if not segs_data:
        cur2.execute(sql_factors, (edge_dt, root_seg_uuid))
        segs_data = cur2.fetchone()[conf.SEGS_DATA_KEY]
        segs_data_cache[(edge_dt, root_seg_uuid)] = segs_data
    ## process specific uuid edge within factors row
    inc_uuid_factors = segs_data[edge_uuid]

    ## DELAY DROP (bigger = more likely)
    delay_drop_pct = inc_uuid_factors['delay_drop_pct']
    delay_drop_points = conf.DELAY_DROP_SCALAR*delay_drop_pct
    inc_dets['incident_points'] += delay_drop_points
    inc_dets['points_breakdown'].append(
        ("Delay drop",
         "{}%".format(round(delay_drop_pct)),
         round(delay_drop_points)))

    ## QUEUE LENGTH (bigger = more likely)
    queue_len = inc_uuid_factors['queue_len']
    if queue_len < conf.LL_NORMAL_QUEUE_LEN:
        queue_len_points = 0
    elif queue_len < conf.LL_HIGHER_QUEUE_LEN:
        queue_len_points = conf.NORMAL_QUEUE_LEN_POINTS
    else:
        queue_len_points = conf.HIGH_QUEUE_LEN_POINTS
    inc_dets['incident_points'] += queue_len_points
    inc_dets['points_breakdown'].append(
        ("Queue length",
         "{:,}m".format(round(queue_len)),
         round(queue_len_points)))

    ## PRECEDING CONGESTION (bigger = more likely)
    prev_uuid = inc_uuid_factors['prev_uuid']
    try:
        preceding_delay = segs_data[prev_uuid]['adjusted_delay_pct']
    except KeyError:  ## no prev_uuid
        inc_dets['points_breakdown'].append(("Preceding congestion", 'N/A',
            'N/A'))
    else:
        preceding_points = preceding_delay/conf.PRECEDING_CONGESTION_DIVISOR  ## divide to weaken effect so not overwhelming other factors
        inc_dets['incident_points'] += preceding_points
        inc_dets['points_breakdown'].append(
            ("Preceding congestion",
             "{:,}%".format(round(preceding_delay)),
             round(preceding_points)))

    ## FOLLOWING DECONGESTION (lower = more likely)
    next_uuid = inc_uuid_factors['next_uuid']  ## could be None
    (decongestion_points, mean_delay_pcts,
     n_following_segs) = _get_decongestion_dets(next_uuid, segs_data)
    inc_dets['points_breakdown'].append(
        ("Following decongestion",
         "avg delay {:,}%<br>(N={})".format(round(mean_delay_pcts),
            n_following_segs),
         round(decongestion_points)))
    inc_dets['incident_points'] += decongestion_points

    ## PRE-INCIDENT FREE-FLOWING
    uuids, unused = seg_helpers.surrounding_uuids(cur2, edge_uuid)
    uuids_clause = seg_helpers.uuids2clause(uuids)
    free_flowing_points, avg_delay_pct = _get_pre_incident_free_flowing_avg(
        cur2, inc_start_dt, uuids_clause)
    inc_dets['points_breakdown'].append(
        ("Pre-incident flow",
         "avg delay {:,}%".format(round(avg_delay_pct)),
         round(free_flowing_points)))
    inc_dets['incident_points'] += free_flowing_points

    ## COMMON ROOT SEG EDGES
    (root_avg_monthly_edge_points,
     root_avg_monthly_edges) = _get_root_avg_monthly_edge(
         cur2, root_seg_uuid)
    inc_dets['points_breakdown'].append(
        ("Rarity of edges",
         "monthly rate for root<br>{:,}".format(round(root_avg_monthly_edges)),
         round(root_avg_monthly_edge_points)))
    inc_dets['incident_points'] += root_avg_monthly_edge_points

    ## COMMON SEG EDGES
    (seg_avg_monthly_edge_points,
     seg_avg_monthly_edges) = _get_seg_avg_monthly_edge(
         cur2, edge_uuid)
    inc_dets['points_breakdown'].append(
        ("Rarity of edges",
         "monthly rate for seg<br>{:,}".format(round(seg_avg_monthly_edges)),
         round(seg_avg_monthly_edge_points)))
    inc_dets['incident_points'] += seg_avg_monthly_edge_points

    ## add TOTAL to report
    inc_dets['points_breakdown'].append(
        ('TOTAL', '', round(inc_dets['incident_points'])))
    return inc_dets

def get_likely_incidents(cur, cur2, debug=False, verbose=False):
    """
    No edge, no incident. It is easy to get massive delay pcts without any
    incidents (feels like major congestion crystalising e.g. in holiday period
    near Huntly etc). So we start from edges.

    Edges are defined in terms of large delays then rapid drop.

    For each edge in edges table, gather together incident details.
    """
    likely_incidents = []
    
    ## process each unique edge uuid-date
    sql_edge_uuid_dates = """\
    SELECT
    uuid,
    date,
    root_seg_uuid
    FROM {edges}
    WHERE is_edge
    ORDER BY root_seg_uuid, date, uuid
    """.format(edges=conf.EDGES_TBL)
    cur.execute(sql_edge_uuid_dates)
    n_edges_checked = 1
    while True:
        edge_row = cur.fetchone()
        if not edge_row:
            if debug: print("Run out of rows")
            break
        if debug:
            print("{} edge records checked ...".format(n_edges_checked),
                end=' ')
        n_edges_checked += 1
        edge_uuid = edge_row['uuid']
        root_seg_uuid = edge_row['root_seg_uuid']
        edge_dt = edge_row['date']
        inc_dets = get_inc_dets(cur2, edge_uuid, root_seg_uuid, edge_dt,
            debug=debug, verbose=verbose)
        if inc_dets is None:
            if debug: print("No incident details available given data")
            continue
        if inc_dets['incident_points'] > conf.MIN_INCIDENT_POINTS:
            likely_incidents.append(inc_dets)
        else:
            if debug: print("Not enough points - only {}".format(
                inc_dets['incident_points']))
    likely_incidents.sort(key=lambda s: s['incident_points'], reverse=True)
    return likely_incidents

def make_specific_reports(cur):
    """
    Make reports without having to work off the top of get_likely_incidents().
    Can look at any date-location combination e.g. any abnormal congestion.
    """
    '''
    edge_uuid = 'b26350bfc1e1c08e6c93d764e676ce26'
    root_seg_uuid = '3b2359c29a818b9138be66d149d73de7'
    inc_start_dt = nz.localize(datetime.datetime(2016, 12, 26, 8, 43, 4, 97000))
    inc_identified_dt = nz.localize(
        datetime.datetime(2016, 12, 26, 8, 52, 43, 391000))
    edge_dt = inc_identified_dt
    inc_dets = get_inc_dets(cur, edge_uuid, root_seg_uuid, edge_dt, debug=False,
        verbose=False)
    points_breakdown = inc_dets['points_breakdown']
    reports.make_report(cur, focus_uuid=edge_uuid, focus_dt=inc_start_dt,
        points_breakdown=points_breakdown, inc_start_dt=inc_start_dt,
        inc_identified_dt=inc_identified_dt,
        time_series_img_limit=TIME_SERIES_IMG_LIMIT,
        seg_series_img_limit=SEG_SERIES_IMG_LIMIT)
    
    edge_uuid = '2c2769bb0f95297933e296eb8c1ba4c0'
    root_seg_uuid = '9f57434b4415740929ecc2c972135214'
    inc_start_dt = nz.localize(datetime.datetime(2017, 1, 19, 17, 34, 47, 890000))
    inc_identified_dt = nz.localize(
        datetime.datetime(2017, 1, 19, 17, 54, 48, 595000))
    edge_dt = inc_identified_dt
    inc_dets = get_inc_dets(cur, edge_uuid, root_seg_uuid, edge_dt, debug=False,
        verbose=False)
    points_breakdown = inc_dets['points_breakdown']
    reports.make_report(cur, focus_uuid=edge_uuid, focus_dt=inc_start_dt,
        points_breakdown=points_breakdown, inc_start_dt=inc_start_dt,
        inc_identified_dt=inc_identified_dt,
        time_series_img_limit=TIME_SERIES_IMG_LIMIT,
        seg_series_img_limit=SEG_SERIES_IMG_LIMIT)
    ## not an edge
    focus_uuid = '2c2769bb0f95297933e296eb8c1ba4c0'
    #root_seg_uuid = '9f57434b4415740929ecc2c972135214'
    edge_dt = nz.localize(
        datetime.datetime(2017, 1, 19, 17, 54, 48, 595000))
    reports.make_report(cur, focus_uuid=focus_uuid, focus_dt=edge_dt,
        points_breakdown=[], inc_start_dt=None,
        inc_identified_dt=None,
        time_series_img_limit=TIME_SERIES_IMG_LIMIT,
        seg_series_img_limit=SEG_SERIES_IMG_LIMIT)
    ## very early morning crash only blocking one lane
    focus_uuid = 'db1af011c32942d3b76aa1049a9adbdc'
    edge_dt = nz.localize(
        datetime.datetime(2017, 3, 24, 5, 24, 47, 85))
    reports.make_report(cur, focus_uuid=focus_uuid, focus_dt=edge_dt,
        points_breakdown=[], inc_start_dt=None,
        inc_identified_dt=None,
        time_series_img_limit=TIME_SERIES_IMG_LIMIT,
        seg_series_img_limit=SEG_SERIES_IMG_LIMIT)
    ## Adele concert during traffic
    focus_uuid = 'd6318b6bac62a2b9f5f944c626498263'
    edge_dt = nz.localize(
        datetime.datetime(2017, 3, 23, 18, 30, 0, 0))
    reports.make_report(cur, focus_uuid=focus_uuid, focus_dt=edge_dt,
        points_breakdown=[], inc_start_dt=None,
        inc_identified_dt=None,
        time_series_img_limit=TIME_SERIES_IMG_LIMIT,
        seg_series_img_limit=SEG_SERIES_IMG_LIMIT)
    ## "2017-03-23 21:11:00+00";"Due To An Earlier Crash At Gillies Ave Offramp There Is Heavy Congestion From St Lukes Eastbound And Ahb Southbound. Expect Delays Or Avoid The Area.";164891;"Road Hazard";"Crash";"01N-0427/02.60-D   Epsom";"SH 1 Gillies Ave Southbound"
    focus_uuid = '93f4bce74893f5909f720bfd70c5f4f6'
    edge_dt = nz.localize(
        datetime.datetime(2017, 3, 23, 21, 11, 0, 0))
    reports.make_report(cur, focus_uuid=focus_uuid, focus_dt=edge_dt,
        points_breakdown=[], inc_start_dt=None,
        inc_identified_dt=None,
        time_series_img_limit=TIME_SERIES_IMG_LIMIT,
        seg_series_img_limit=SEG_SERIES_IMG_LIMIT)'''
    ## Heavy congestion according to long range context time series chart
    focus_uuid = 'caaad21d8f4d616ede3443041f9205d0'
    edge_dt = nz.localize(
        datetime.datetime(2017, 3, 24, 15, 18, 49, 0))
    reports.make_report(cur, focus_uuid=focus_uuid, focus_dt=edge_dt,
        points_breakdown=[], inc_start_dt=None,
        inc_identified_dt=None,
        time_series_img_limit=TIME_SERIES_IMG_LIMIT,
        seg_series_img_limit=SEG_SERIES_IMG_LIMIT)

def make_likely_incident_reports(cur, cur2, max_reports, debug=False,
        verbose=False):
    likely_incidents = get_likely_incidents(cur, cur2, debug, verbose)
    if debug:
        pp(likely_incidents, width=100)
        with open('incident_data.txt', 'w') as f:
            f.write(pf(likely_incidents))
    for n, likely_incident in enumerate(likely_incidents, 1):
        reports.make_report(cur,
            likely_incident['edge_uuid'],
            likely_incident['edge_dt'],
            likely_incident['points_breakdown'],
            likely_incident['inc_start_dt'],
            likely_incident['inc_identified_dt'],
            TIME_SERIES_IMG_LIMIT, SEG_SERIES_IMG_LIMIT)
        if n >= max_reports:
            print("Made maximum number of reports allowed by max_reports "
                "setting ({})".format(max_reports))
            break

def main():
    pwd = not4git.access_local  #getpass.getpass('Enter postgres password: ')
    con = pg.connect("dbname='traffic' "
        "user='postgres' "
        "host='localhost' "
        "port=5433 "
        "password='{}'".format(pwd))
    cur = con.cursor(cursor_factory=pg_extras.DictCursor)
    cur2 = con.cursor(cursor_factory=pg_extras.DictCursor)  ## if I need to do something inside a loop of fetchmany() etc
    
    #pp(seg_helpers.root_route_ids2uuids(cur, root_route_ids=[333, 366]))
    
    #make_src_data.make_tbls(con, cur, cur2)

    make_specific_reports(cur)

    #uuids = seg_helpers.get_terminal_uuids4route_id(cur, root_route_id=333) #333, 366
    #report_maps.basic_map_segs(cur, uuids)

    #make_likely_incident_reports(cur, cur2, max_reports=MAX_REPORTS, debug=True,
    #    verbose=False)


main()
