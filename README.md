# README #

### What is this repository for? ###

* Identifying congestion that is most incident-like
* Visualising events (specific datetime and road segment) 

### How do I get set up? ###

* Need Postgresql on local system
* Three tables are needed from transport product database
  * routes
  * segments
  * segment_traffic_data
* The file to run is identify_delays. Comment in and out within main() and change settings at top of module such as MAX_REPORTS, TIME_SERIES_IMG_LIMIT, and SEG_SERIES_IMG_LIMIT

### Who do I talk to? ###

* Grant Paton-Simpson (Grant.PatonSimpson@qrious.co.nz)

### Example output (congestion but non-incident) ###

![example_output_not_incident.png](https://bitbucket.org/repo/EjrGbM/images/3740203140-example_output_not_incident.png)