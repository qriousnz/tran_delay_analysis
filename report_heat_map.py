#! /usr/bin/env python3

from pprint import pprint as pp

import report_helpers
import seg_helpers

def _val2heat_color(val):
    """
    white if not containing data, green if as expected, orange if nearly as
    expected, and red if not as expected.
    """
    if val is '':
        color = 'white'
    elif val < 50:
        color = 'bright-green'
    elif val < 100:
        color = 'green'
    elif val < 250:
        color = 'light-orange'
    elif val < 500:
        color = 'mid-orange'
    elif val < 900:
        color = 'red-orange'
    elif val < 1200:
        color = 'light-red'
    elif val < 2000:
        color = 'bright-red'
    else:
        color = 'lurid-red'
    return color

def _vals2colour_class_and_vals(vals):
    items = []
    for val in vals:
        items.extend([_val2heat_color(val), val])
    return items

def get_expected_congestion_chart(cur, focus_dt, dates_clause, uuid_dets, uuids,
        uuids_clause, is_inc=True):
    """
    Heatmap with date rows and seg uuid (spatial) columns.
    """
    N_PREV = 8
    N_POST = 6
    inc_data_idx = round(len(uuids)/2)  ## always in middle (zero-based of course)
    date_offsets = range(-N_PREV, 1 + N_POST)
    dates2use = report_helpers.get_snapshot_dates(cur, focus_dt, dates_clause,
        uuids, uuids_clause, date_offsets)
    seg_lbls = []
    for uuid_det in uuid_dets:
        seg_lbls.append(seg_helpers.get_seg_heading(uuid_det['heading'],
            uuid_det['sequence_id']))
    segs_history = []
    for date in dates2use:
        snapshot_data = report_helpers.get_snapshot_data(cur, date,
            uuids_clause)
        timestr = date.strftime('%-I:%M:%S %p').lower()
        datestr = date.strftime('%b-%d')
        vals = [round(x['increase']) for x in snapshot_data]
        segs_history.append({'time': timestr, 'date': datestr, 'vals': vals})
    N_EXTRA_ROWS = 5
    html = ["<h2>Expected overall pattern of delay percentages?</h2>"]
    n_data_cols = len(seg_lbls)
    n_data_rows = len(segs_history)
    std_row_lbl_cell_tpl = (
        "<td class='row-lbl'><span class='date'>{}</span>"
        "<br><span class='time'>{}</span></td>")
    std_data_cell_tpl = "<td class='{}'>{}</td>"
    row_tpl = ("<tr>" + std_row_lbl_cell_tpl + std_data_cell_tpl*n_data_cols
        + "</tr>")
    html.append("<table id='heat-colour'>\n<tbody>\n<tr>\n<td>")  ## heat map and colour map table
    html.append("<table id='heat'>")  ## heat map only
    html.append("<thead>\n<tr>\n<th></th><th></th>"
        + '\n'.join("<th class='lbl'>{}</th>".format(seg_lbl)
            for seg_lbl in seg_lbls)
        + "</tr>\n</thead>")
    html.append("<tbody>"
        "\n<tr>\n<td class='yaxis' rowspan={}>"
        "<div class='yaxis_lbl'>Polling snapshots</div></td>\n</tr>"
        .format(n_data_rows+N_EXTRA_ROWS))
    for n, row_dic in enumerate(segs_history, 1):
        timestr = "\N{DOWNWARDS BLACK ARROW}{}".format(row_dic['time'])
        datestr = row_dic['date']
        vals = row_dic['vals']
        incident_row = (n == N_PREV + 1)
        if not incident_row:
            html.append(row_tpl.format(timestr, datestr,
                *_vals2colour_class_and_vals(vals)))
        else:
            n_pre_cols = inc_data_idx
            n_post_cols = n_data_cols - n_pre_cols - 1
            if is_inc:
                ## comment above special incident row
                html.append("<tr>"
                    "<td></td>"
                    "<td colspan={} class='comment'>Everything above should be "
                    "pale green i.e. low congestion because the alleged incident "
                    "hasn't even happened yet</td></tr>".format(n_data_cols))
                datestr = ("{}<br>"
                    "<span class='incident-lbl'>Time of incident?</span>"
                    .format(datestr))
                ## special incident row
                html.append(("<tr>"
                + std_row_lbl_cell_tpl
                + std_data_cell_tpl*n_pre_cols
                + "<td class='incident {}'>"
                "<span class='large-symbol'>\N{WARNING SIGN}"
                "</span>&nbsp;&nbsp;&nbsp;&nbsp;{}</td>"
                + std_data_cell_tpl*n_post_cols
                + "</tr>").format(
                    timestr, datestr, *_vals2colour_class_and_vals(vals)))
                ## comments below incident
                html.append("<tr>"
                    "<td></td>"
                    + "<td colspan={} class='comment'><span class='symbol'>"
                    .format(n_pre_cols)
                    + "</span>Traveling towards incident <span class='symbol'>"
                    + "\N{BLACK RIGHTWARDS ARROW}"
                    + "</span><br>Should be towards red end because of traffic "
                    "backing up</td>"
                    + "<td class='comment'>"
                    "<span class='incident-lbl'>Incident location?</span></td>"
                    "<td colspan={} class='comment'><span class='symbol'>"
                    .format(n_post_cols)
                    + "</span>Traveling away from incident "
                    "<span class='symbol'>\N{BLACK RIGHTWARDS ARROW}</span><br>"
                    "Should be green because free-flowing if after an incident"
                    "</td>")
            else:
                html.append(("<tr class='focus-row'>"
                + std_row_lbl_cell_tpl
                + std_data_cell_tpl*n_pre_cols
                + "<td class='incident {}'>{}</td>"  ## colour class and val for middle
                + std_data_cell_tpl*n_post_cols
                + "</tr>").format(
                    timestr, datestr, *_vals2colour_class_and_vals(vals)))
    ## add post-incident comments
    if is_inc:
        html.append("<tr><td>&nbsp;</td><td colspan={} class='comment'>"
            .format(n_pre_cols)
            + "Should see increasing leftwards movement of red/orange as we "
            "move downwards and traffic queues form in front of incident</td>"
            "<td>&nbsp;</td>"
            "<td colspan={} class='comment'>Should still see mainly green, "
            "free-flowing traffic further along road from incident</td></tr>"
            .format(n_post_cols))
    ## add lower seg labels
    html.append("<tr><td>&nbsp;</td>"
        + '\n'.join("<td class='lbl'>{}</td>".format(seg_lbl)
        for seg_lbl in seg_lbls) + "</tr>")
    ## x axis title
    html.append("<tr><td>&nbsp;</td>"
        "<td colspan={} class='xaxis_lbl'>Road segments</td></tr>"
        .format(n_data_cols))
    html.append("</tbody>\n</table>")
    html.append("</td>")
    html.append("\n<td id='right-cell'>")
    html.append("<table id='colour-map'>\n<thead>"
        "\n<tr>\n<th colspan=2 id='colour-map-col-heading'>Delay %</th>"
        "\n</tr>\n</thead>\n<tbody>")
    mappings = [
        ('&ge; 2000', 'lurid-red'),
        ('1200-1999', 'bright-red'),
        ('900-1199', 'light-red'),
        ('500-899', 'red-orange'),
        ('250-499', 'mid-orange'),
        ('100-249', 'light-orange'),
        ('50-99', 'green'),
        ('&lt; 50', 'bright-green'),
    ]
    for mapping in mappings:
        html.append("<tr>\n<td class='heatmap-text'>{}</td>"
            "<td class='{} heatmap-color'>&nbsp;&nbsp;&nbsp;</td>"
            "\n</tr>".format(*mapping))
    html.append("</tbody></table>")  ## colour map table
    html.append("</td>\n</tr>\n</tbody>\n</table>")  ## heat map and colour map
    return html
