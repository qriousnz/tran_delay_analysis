#! /usr/bin/env python3

import json
from pprint import pprint as pp
from statistics import mean

import psycopg2 as pg

import conf
import seg_helpers

def _audit_edge_dets(seg_uuid, date, segs, pre=True):
    msg = ("pre segs (should be HIGHer delay increases)" if pre
       else "post segs (should be LOWer delay increases)")
    comp_uuids = [seg['uuid'] for seg in segs]
    uuids = comp_uuids + [seg_uuid, ]
    uuids_clause = seg_helpers.uuids2clause(uuids)
    sql_audit_comp_segs = """\
    /* {msg} */
    SELECT *,
      uuid = '{seg_uuid}' AS
    is_seg_uuid
    FROM {traffic_final}
    WHERE date = '{date}'
    AND {uuids_clause}
    """.format(msg=msg, seg_uuid=seg_uuid, traffic_final=conf.TRAFFIC_FINAL_TBL,
        uuids_clause=uuids_clause, date=date.isoformat())
    print(sql_audit_comp_segs)

def _get_pre_adjusted_delay_pct(segs_traffic_dets, seg_idx):
    """
    Unlike post check we want to detect incident even if it hasn't congested
    multiple segments (quite yet).
    """
    pre_segs = []
    for pre_idx in range(seg_idx-2, seg_idx):
        try:
            pre_segs.append(segs_traffic_dets[pre_idx])
        except IndexError:
            break
    if len(pre_segs) == 2:
        pre_adjusted_delay_pct = mean([pre_seg['adjusted_delay_pct']
            for pre_seg in pre_segs])
    else:
        pre_adjusted_delay_pct = None
    return pre_adjusted_delay_pct, pre_segs

def _get_post_adjusted_delay_pct(segs_traffic_dets, seg_idx):
    """
    Try to get a decent number of segments forwards. There should be any severe
    congestion for a long way after a genuine incident (as opposed to mere
    crystalising congestion out of thick traffic)
    """
    post_segs = []
    for post_idx in range(seg_idx+1, seg_idx+11):  ## approx 10*50m
        try:
            post_segs.append(segs_traffic_dets[post_idx])
        except IndexError:
            break
    if len(post_segs) < 2:
        post_adjusted_delay_pct = None
    else:
        post_adjusted_delay_pct = mean([post_seg['adjusted_delay_pct']
            for post_seg in post_segs])
    return post_adjusted_delay_pct, post_segs

def _edge_dets(segs_traffic_dets, seg_idx, date, audit=False):
    """
    An edge is defined as a segment where the following all apply:
    1) the mean delay increase for the segment's predecessors is a lot higher
    than that of the following segments. Need a decent number of segments
    otherwise can be caught out by stable dips that are localised e.g.
    
    | * *         *   *   * 
    | * *         *   * * *
    | * * *       * * * * * *
    | * * * *   * * * * * * * 
    | * * * * * * * * * * * *
           \
            \
             not an edge even though prev 2 high, next 2 low, and alleged edge
             in between. Need to look ahead more.
    
    2) the segment has an increase in the middle (because if the incident is
    inside a segment its delay increase will be a mix of really high for the
    part of the segment pre-incident and quite low just after, thus an in-
    between overall result depending on how close the incident is to the start
    or the end of the segment.
    3) the pre_adjusted_delay_pct passes a threshold of significance to ensure
    big proportional drops from small base levels are ignored.
    
          1000 % -|          *
                  |          *
                  |    *     *
                  |    *     *
                  |    *     *         looks like an edge (in the middle of the segment)
    increase      |    *     *        /
    over          |    *     *       /
    expected %    |    *     *     *
                  |    *     *     *
                  |    *     *     *
                  |    *     *     *     *     *
          0 % pts -------------------------------
                  seg  1     2     3     4     5

    pre = 1000 ((900+1100)/2)
    post = 100 ((100+100)/2)
    seg = 400
    100 < 400 < 1000
    so seg 3 looks like an edge in this snapshot. 

    Returns None if unable to calculate (because too close to start or end of
    root segment so unable to get averages).
    """
    seg_uuid = segs_traffic_dets[seg_idx]['uuid']
    pre_adjusted_delay_pct, pre_segs = _get_pre_adjusted_delay_pct(
        segs_traffic_dets, seg_idx)
    post_adjusted_delay_pct, post_segs = _get_post_adjusted_delay_pct(
        segs_traffic_dets, seg_idx)
    seg_adjusted_delay_pct = segs_traffic_dets[seg_idx]['adjusted_delay_pct']
    if (pre_adjusted_delay_pct is not None
            and post_adjusted_delay_pct is not None
            and pre_adjusted_delay_pct != 0):  ## !=0 to avoid zero division error
        delay_drop_pct = (100*(pre_adjusted_delay_pct - post_adjusted_delay_pct)
            /pre_adjusted_delay_pct)
        seg_in_middle = \
            (post_adjusted_delay_pct < seg_adjusted_delay_pct < pre_adjusted_delay_pct)
        edge = (delay_drop_pct >= conf.EDGE_DROP_PCT_THRESHOLD
            and seg_in_middle
            and pre_adjusted_delay_pct >= conf.EDGE_ABS_PRE_INCREASE)
        if edge and audit:
            print("delay_drop_pct: {delay_drop_pct}"
                "\npre_adjusted_delay_pct: {pre_adjusted_delay_pct}"
                "\npost_adjusted_delay_pct: {post_adjusted_delay_pct}"
                "\nseg_adjusted_delay_pct:{seg_adjusted_delay_pct}".format(
                delay_drop_pct=delay_drop_pct,
                pre_adjusted_delay_pct=pre_adjusted_delay_pct,
                post_adjusted_delay_pct=post_adjusted_delay_pct,
                seg_adjusted_delay_pct=seg_adjusted_delay_pct))
            _audit_edge_dets(seg_uuid, date, segs=pre_segs, pre=True)
            _audit_edge_dets(seg_uuid, date, segs=post_segs, pre=False)
        delay_drop_pct = round(delay_drop_pct)
    else:
        edge = False
        delay_drop_pct = None
    return edge, delay_drop_pct

def _get_queue_dets(segs_traffic_dets,
        threshold=conf.QUEUE_CONGESTION_THRESHOLD_PCT):
    """
    For each segment, record preceding queue length. No requirement that queue
    terminates with the segment. Queues are defined as contiguous congestion.

    Queue length doesn't care about increased levels of congestion - just the
    presence of very slow-moving traffic. So using unadjusted_delay_pct not
    adjusted_delay_pct.Note - a queue might just be one segment long.
    
    - - -   - -       
    -x
    - -x
    - - -x
           x
            -x
            - -x etc
    """
    ## start at beginning and loop until hitting non-congested seg.
    queue_dets = {}
    queue_len = 0
    for seg_traffic_dets in segs_traffic_dets:
        uuid = seg_traffic_dets['uuid']
        congested = seg_traffic_dets['unadjusted_delay_pct'] >= threshold
        if congested:  ## add to open queue and keep going
            seg_len = seg_traffic_dets['len_metres']
            queue_len += seg_len
        else:
            queue_len = 0
        queue_dets[uuid] = queue_len
    return queue_dets

def _process_factor_data(con, cur2, date, root_seg_uuid, hits_only=False,
        audit=False):
    """
    For each date and root_seg_uuid collect traffic data for all terminal
    segments in order. Process looking for key patterns.

    Populate seg_dets and look for edges to add to edges.

    To look for edges, move through segments in order. Look at avg of seg + n
    previous cf next n segs. A significant drop (large enough to indicate an
    incident edge)?

    Note - need to avoid using the cursor being used in the outer loop calling
    this function as we'll blow up next call to cur.fetchmany() if we do ;-).
    
    We also need to populate an edges table so we can later calculate monthly
    event rates.
    """
    sql_seg_uuids = """\
    SELECT traffic.*
    FROM {segdets} AS segs
    INNER JOIN
    {traffic_final} AS traffic
    USING(uuid)
    WHERE segs.root_seg_uuid = %s
    AND segs.level = %s
    AND traffic.date = %s
    ORDER BY sequence_id
    """.format(segdets=conf.SEGDETS_TBL, traffic_final=conf.TRAFFIC_FINAL_TBL)
    cur2.execute(sql_seg_uuids, (root_seg_uuid, conf.TERMINAL_LEVEL, date))
    segs_traffic_dets = cur2.fetchall()
    queue_dets = _get_queue_dets(segs_traffic_dets,
        threshold=conf.QUEUE_CONGESTION_THRESHOLD_PCT)
    any_edges = False
    segs_data = {}
    for seg_idx in range(len(segs_traffic_dets)):
        seg_dict = {}
        seg_traffic_dets = segs_traffic_dets[seg_idx]
        seg_uuid = seg_traffic_dets['uuid']
        ## edge? ***************************************************************
        edge, delay_drop_pct = _edge_dets(segs_traffic_dets, seg_idx, date,
            audit)
        is_edge = bool(edge)
        if is_edge:
            any_edges = True
        ## While we're here, populate edges table for efficiency
        sql_insert_event = """\
        INSERT INTO {edges}
        (root_seg_uuid, uuid, date, is_edge)
        VALUES (%s, %s, %s, %s)
        """.format(edges=conf.EDGES_TBL)
        cur2.execute(sql_insert_event, (root_seg_uuid, seg_uuid, date, is_edge))
        ## seg_data ************************************************************
        seg_dict['is_edge'] = is_edge
        seg_dict['delay_drop_pct'] = delay_drop_pct
        seg_dict['queue_len'] = queue_dets[seg_uuid]
        seg_dict['adjusted_delay_pct'] = round(
            seg_traffic_dets['adjusted_delay_pct'])
        if seg_idx > 0:
            prev_uuid = segs_traffic_dets[seg_idx-1]['uuid']  ## can't look for IndexError because negative values can be valid indexes
        else:
            prev_uuid = None
        seg_dict['prev_uuid'] = prev_uuid  ## enable us to navigate around without having to ensure order and use indexes etc.
        try:
            next_uuid = segs_traffic_dets[seg_idx+1]['uuid']
        except IndexError:
            next_uuid = None
        seg_dict['next_uuid'] = next_uuid
        segs_data[seg_uuid] = seg_dict
    if hits_only:
        if not any_edges:
            return  ## i.e. don't add record
    json_segs_data = json.dumps(segs_data)
    sql_insert = """\
    INSERT INTO {factors_root_date}
    (root_seg_uuid, date, segs_data)
    VALUES (%s, %s, %s)
    """.format(factors_root_date=conf.FACTORS_ROOT_DATE_TBL)
    try:
        cur2.execute(sql_insert, (root_seg_uuid, date, json_segs_data))
    except pg.ProgrammingError as e:
        print(e)
        print(sql_insert)
        print(date)
        print(root_seg_uuid)
        pp(json_segs_data)
