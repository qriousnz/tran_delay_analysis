#! /usr/bin/env python3

from datetime import timedelta

import conf

def get_surrounding_dates(date, hours_either_side=conf.HOURS_EITHER_SIDE):
    debug = False
    start_dt = date - timedelta(hours=hours_either_side)
    end_dt = date + timedelta(hours=hours_either_side)
    if debug:
        print("date {}\nstart_dt {}\nend_dt {}".format(date, start_dt, end_dt))
    return start_dt, end_dt

def date_range2clause(start_date, end_date, is_str=False):
    """Takes either datetimes or iso8601 strings"""
    if is_str:
        start_date = start_date.isoformat()
        end_date = end_date.isoformat()
    dates_clause = "date BETWEEN '{}' AND '{}'".format(start_date, end_date)
    return dates_clause

def get_surrounding_dates_clause(date,
        hours_either_side=conf.HOURS_EITHER_SIDE):
    """Get date clause ready to add after WHERE"""
    start_dt, end_dt = get_surrounding_dates(date, hours_either_side)
    dates_clause = date_range2clause(start_dt, end_dt, is_str=False)
    return dates_clause

def date_ranges_overlap(start_dt, end_dt, oth_start_dt, oth_end_dt):
    """
    Overlap if other date range ends before the reference date range starts or
    starts after the reference date range ends.
    """
    overlap = (oth_end_dt >= start_dt) and (oth_start_dt <= end_dt)
    return overlap
    