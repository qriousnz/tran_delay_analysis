#! /usr/bin/env python3

"""
Get segments data and translate it into a usable form for detecting anomalous
congestion.

Note - need to filter out road transport segments so we can focus on incident
segments only. There is only one level below the road transport routes so easy
to eliminate.

Focus on one top-level route and all its descendants - in both directions. E.g.
routes 30 and 82 (the reverse of 30).

Step 1 - Copy routes and segments tables to local traffic db
Step 2 - Create list of seg_uuids to filter segment_traffic_data by
Step 3 - Copy filtered subset of segment_traffic_data to local traffic db

All routes are linear and do not split etc.

All routes are broken into same number of levels. Level 0, 1, 2.

Easier to extract data using Python rather than complex SQL so we can easily
move up and down data hierarchies.

Need to get all segment uuids, their levels, and their ancestor, peer, and child
relationships. Also need to know order of final level segments so we can
determine earlier and later segments.

We can't easily do matches in the opposite direction (using postgis tricks
such as ST_LENGTH(ST_INTERSECTION(...)) because they are too slow and we have
to add lots of margin for error e.g. many reversed segments are offset e.g.
left side of road vs right side of road. Given our purpose is to identify
segments to chart around a core one for the purposes of context it is OK to
use a more approximate approach and get that bunch.

Once we have this metadata on segments we can process traffic data for those
segments.

Route route_id 30 is the start of the Huntly segments and is rather glamorously
labelled the Thermal Explorer Highway (LOL). Another Level 0 route (under tags)
has that same name but with reverse on end. Also use that set of segments

Only one segment has that route_id as parent - namely
"22185969033bf643e963b3ec7ca18de9" so we can create tree using this segment as
starting point. segments.ID is in the reverse order segments appear on roads
(Johannes pers. comm. 27/1/2017).


segments.data
---------------------------------------------------
"{"summary":"Deans Ave",
  "bounds":{"south":-43.5282859,"west":172.6123569,"north":-43.527837,"east":172.6124006},
  "origin_position":{"lat":-43.52784,"lng":172.6124},
  "destination_position":{"lat":-43.52829,"lng":172.61236},
  "overview_polyline":"~othGojp|_@xAF",
  "origin_address":"167 Deans Ave, Riccarton, Christchurch 8011, New Zealand",
  "destination_address":"163 Deans Ave, Riccarton, Christchurch 8011, New Zealand"}"


unadjusted_delay_pct -- actual difference between duration without traffic but
at legal speed limits (duration) and the actual duration (duration_in_traffic)
in traffic right now e.g. usually 5s but now 25s because rush hour.

avg_delay_pct -- median duration difference for this segment for this time of
day e.g. usually 4 times as long (20s) as typical because rush hour so might be
300%. 5--> 20 = 15 up = 3x i.e. +300% increase

adjusted_delay_pct -- like unadjusted_delay_pct but takes into account time of
day etc e.g. weekday peak rush hour.

* Getting source data locally *****************

Need to access product database over VPN


"""
import json
from pprint import pprint as pp
import psycopg2 as pg
import psycopg2.extras as pg_extras

import conf
import factors
import not4git
import seg_helpers

sql_update_extras_tpl = """\
    UPDATE {tbl}
    SET
    level = %s,
    reverse = %s,
    root_seg_uuid = %s
    WHERE uuid = %s
    """.format(tbl=conf.SEGDETS_TBL)

## duration is the average time expected, duration_in_traffic is how long it will actually take given current traffic
## unadjusted_delay_pct is the size of the change in duration between typical and actual
raw_duration_diff_fields_from_durs = """\
  duration,
  duration_in_traffic,
    100*((duration_in_traffic - duration)/CAST(duration AS Float)) AS
  unadjusted_delay_pct
"""
## HOUR takes hour from timezone-aware date as at the timezone you are set for (and as the data displays)
## e.g. 2016-12-13 06... --> 6
## so if timezone is set to 'NZ' then all is well (for postgresql - matplotlib needs its own setting to use Pacific/Auckland timezone)
## if you set different timezone you'll see the time relative to that new timezone e.g. Asia/Shanghai
## sudo vim /etc/postgresql/9.5/main/postgresql.conf
## sudo /etc/init.d/postgresql start

raw_date_fields_from_date = """\
  date,
    EXTRACT(ISODOW FROM date) IN (6,7) AS
  weekend,
    EXTRACT(HOUR FROM date) AS
  hour,
    EXTRACT(MINUTE FROM date) AS
  minute
"""
date_group_id_field_from_raw_date_fields = """\
(CASE
  WHEN weekend THEN
    CASE
      WHEN hour < 19 THEN 8 /* 8 weekend day */
      ELSE 9 /* weekend night */
      END
  ELSE
    CASE
      WHEN ((hour < 7) OR (hour = 7 AND minute < 30)) THEN 1 /* week early */
      WHEN ((hour < 9) OR (hour = 9 AND minute < 30)) THEN 2 /* week morning rush */
      WHEN ((hour < 15) OR (hour = 15 AND minute < 30)) THEN 3 /* week middle day */
      WHEN hour < 17 THEN 4 /* week school rush */
      WHEN hour < 18 THEN 5 /* week peak rush */
      WHEN ((hour < 19) OR (hour = 19 AND minute < 30)) THEN 6 /* week late rush */
      ELSE 7 /* week evening night */ 
    END
END) AS
date_group_id
"""

def _drop_tbl(con, cur, tbl):
    sql_drop_tbl = """\
    DROP TABLE IF EXISTS {tbl}
    """.format(tbl=tbl)
    cur.execute(sql_drop_tbl)
    con.commit()

def make_reverses_tbl(con, cur):
    """Make table mapping root seg uuids to reverse version"""
    _drop_tbl(con, cur, tbl=conf.REVERSES_TBL)
    sql_make_tbl = """\
    CREATE TABLE {tbl}
    (
      root_seg_uuid text NOT NULL,
      reverse_root_seg_uuid text NOT NULL,
      PRIMARY KEY (root_seg_uuid)
    )
    """.format(tbl=conf.REVERSES_TBL)
    cur.execute(sql_make_tbl)
    con.commit()
    origs_to_reverses = conf.ROUTE_SRC_IDS
    reverses_to_orig = [(x[1], x[0]) for x in conf.ROUTE_SRC_IDS]
    route_ids2reverses = origs_to_reverses + reverses_to_orig
    route_ids = [x[0] for x in route_ids2reverses]
    filter_clause = "(" + ",".join([str(route_id) for route_id in route_ids]) + ")"
    sql_routes_ids = """\
    SELECT route_id, uuid
    FROM {segments}
    WHERE route_id IN {filter_clause}
    """.format(segments=conf.SEGS_TBL, filter_clause=filter_clause)
    cur.execute(sql_routes_ids)
    route_id2uuid = dict(cur.fetchall())
    subclause_data = []
    for route_id, reverse_route_id in route_ids2reverses:
        try:
            root_seg_uuid = route_id2uuid[route_id]
            reverse_root_seg_uuid = route_id2uuid[reverse_route_id]
        except KeyError:
            pass
        else:
            subclause_data.append((root_seg_uuid, reverse_root_seg_uuid))
    subclauses = [
        "('{root}', '{reverse}')".format(
            root=root_seg_uuid, reverse=reverse_root_seg_uuid)
        for root_seg_uuid, reverse_root_seg_uuid in subclause_data]
    clauses = ",\n".join(subclauses)
    sql_populate_tbl = """\
    INSERT INTO {tbl}
    (root_seg_uuid, reverse_root_seg_uuid) VALUES
    {clauses}
    """.format(tbl=conf.REVERSES_TBL, clauses=clauses)
    cur.execute(sql_populate_tbl)
    con.commit()
    print("Made reverses table")

def init_seg_dets_tbl(con, cur):
    """
    Make initial version of table containing useful segment details
    including items pulled out of the json so easier to work with.

    Excludes road transport segments.
    """
    _drop_tbl(con, cur, tbl=conf.SEGDETS_TBL)
    sql_make_tbl = """\
    CREATE TABLE {tbl} (
      uuid text NOT NULL,
      len_metres real,
      heading text,
      id integer,
      level integer,
      root_seg_uuid text,
      sequence_id integer,
      reverse boolean,
      origin_address text,
      dest_address text,
      origin_coords point,
      dest_coords point,
      parent_uuid text,
      route_id integer
    )
    """.format(tbl=conf.SEGDETS_TBL)
    cur.execute(sql_make_tbl)
    sql_set_prim_key = """\
    ALTER TABLE ONLY {tbl}
    ADD CONSTRAINT segdets_pkey PRIMARY KEY (uuid)
    """.format(tbl=conf.SEGDETS_TBL)
    cur.execute(sql_set_prim_key)
    ## -> get array, ->> get value (see https://www.postgresql.org/docs/9.5/static/functions-json.html)
    sql_populate_tbl = """\
    INSERT INTO {dest_tbl} SELECT
      uuid,
        100000*ST_LENGTH(geom) AS
      len_metres,
      heading,
      id,
        NULL AS
      level,
        NULL AS
      root_seg_uuid,  /* Only populated for terminal segments */
      sequence_id,
        NULL AS
      reverse,
        data->>'origin_address' AS
      origin_address,
        data->>'destination_address' AS
      dest_address,
        Point(
          CAST(data->'origin_position'->>'lat' AS Float),
          CAST(data->'origin_position'->>'lng' AS Float)) AS
      origin_coords,
        Point(
          CAST(data->'destination_position'->>'lat' AS Float),
          CAST(data->'destination_position'->>'lng' AS Float)) AS
      dest_coords,
      parent_uuid,
      route_id
    FROM {src_tbl}
    WHERE route_id >= %s
    """.format(src_tbl=conf.SEGS_TBL, dest_tbl=conf.SEGDETS_TBL)
    cur.execute(sql_populate_tbl, (conf.MIN_INCIDENT_ROUTE_ID, ))
    con.commit()

def _add_extras2children(con, cur, root_seg_uuid, parent_seg_uuid, level,
        reverse):
    """
    Populate level, reverse, root_seg_uuid fields for child segments.

    sequence_id is low to high so segments are in order while pointing in correct
    direction.
    """
    sql_get_children = """\
    SELECT uuid
    FROM {tbl}
    WHERE parent_uuid = %s
    ORDER BY sequence_id /* so we can add simple order counter */
    """.format(tbl=conf.SEGDETS_TBL)
    cur.execute(sql_get_children, (parent_seg_uuid, ))
    child_seg_uuids = [x[0] for x in cur.fetchall()]
    next_level = level + 1
    for child_seg_uuid in child_seg_uuids:
        cur.execute(sql_update_extras_tpl,
            (level, reverse, root_seg_uuid, child_seg_uuid))
        con.commit()
        _add_extras2children(con, cur, root_seg_uuid,
            parent_seg_uuid=child_seg_uuid, level=next_level, reverse=reverse)

def add_extra_dets(con, cur, root_route_id, reverse):
    """Populate level, reverse, root_seg_uuid fields for route segments"""
    sql_get_root_seg_uuid_tpl = """\
    SELECT uuid
    FROM {tbl}
    WHERE route_id = %s
    """.format(tbl=conf.SEGDETS_TBL)
    cur.execute(sql_get_root_seg_uuid_tpl, (root_route_id, ))
    seg_row = cur.fetchone()
    if not seg_row:  ## e.g. 299;"{IR,"Newtown Wellington","Level 0","Section 4.1",reverse}";2;;"{"tags": ["IR", "Newtown Wellington", "Level 0", "Section 4.1", "reverse"]}" has no segments
        return
    root_seg_uuid = seg_row['uuid']
    this_level = 0
    uuid2update = root_seg_uuid
    cur.execute(sql_update_extras_tpl,
        (this_level, reverse, root_seg_uuid, uuid2update))
    con.commit()
    next_level = 1
    _add_extras2children(con, cur, root_seg_uuid, parent_seg_uuid=root_seg_uuid,
        level=next_level, reverse=reverse)

def _wipe_lacking_level(con, cur):
    sql_del = """\
    DELETE FROM {tbl}
    WHERE level IS NULL
    """.format(tbl=conf.SEGDETS_TBL)
    cur.execute(sql_del)

def make_seg_dets_tbl(con, cur):
    """
    Make table containing all useful segment details including level and misc
    items pulled out of the json so easier to work with.
    """
    init_seg_dets_tbl(con, cur)
    for root_route_id, reverse_root_route_id in conf.ROUTE_SRC_IDS:
        add_extra_dets(con, cur, root_route_id, reverse=False)
        add_extra_dets(con, cur, reverse_root_route_id, reverse=True)
    _wipe_lacking_level(con, cur)
    print("Made segdets table")

def _get_child_dets(cur, parent_uuid):
    """
    Get child details for parent uuid.
    """
    sql_get_recs = """\
    SELECT uuid, level
    FROM {tbl}
    WHERE parent_uuid = %s
    """.format(tbl=conf.SEGDETS_TBL)
    cur.execute(sql_get_recs, (parent_uuid, ))
    child_dets = cur.fetchall()
    return child_dets

def _get_terminal_uuids(cur, parent_uuid):
    """
    Get terminal uuids for a uuid.
    """
    child_dets = _get_child_dets(cur, parent_uuid)
    if child_dets:
        terminal_uuids = []
        for child_uuid, level in child_dets:
            if level == conf.TERMINAL_LEVEL:
                terminal_uuids.append(child_uuid)
            else:
                terminal_uuids.extend(_get_terminal_uuids(cur, child_uuid))
    else:
        return [parent_uuid, ]  ## heh - not really a parent
    return terminal_uuids

def _get_uuid2terminals(con, cur):
    """
    For every uuid get its terminal uuids (which are itself if it is a terminal
    uuid i.e. has a level which is the smallest level)
    """
    sql_get_uuids = "SELECT uuid FROM {tbl}".format(tbl=conf.SEGDETS_TBL)
    cur.execute(sql_get_uuids)
    uuid2terminals = []
    for row in cur.fetchall():
        uuid = row[0]
        terminal_uuids = _get_terminal_uuids(cur, uuid)
        for terminal_uuid in terminal_uuids:
            uuid2terminals.append((uuid, terminal_uuid))
    return uuid2terminals

def make_uuid2terminals_tbl(con, cur):
    """Make table linking uuids to terminal (level 2) uuids"""
    _drop_tbl(con, cur, tbl=conf.TERMINALS_TBL)
    sql_make_tbl = """\
    CREATE TABLE {tbl} (
      id int NOT NULL,
      uuid text NOT NULL,
      terminal_uuid text NOT NULL
    )
    """.format(tbl=conf.TERMINALS_TBL)
    cur.execute(sql_make_tbl)
    sql_set_prim_key = """\
    ALTER TABLE ONLY {tbl}
    ADD CONSTRAINT terminals_pkey PRIMARY KEY (id)
    """.format(tbl=conf.TERMINALS_TBL)
    cur.execute(sql_set_prim_key)
    sql_add_uuid_index = """\
    CREATE INDEX ON {tbl} (uuid)
    """.format(tbl=conf.TERMINALS_TBL)
    cur.execute(sql_add_uuid_index)
    sql_add_terminal_index = """\
    CREATE INDEX ON {tbl} (terminal_uuid)
    """.format(tbl=conf.TERMINALS_TBL)
    cur.execute(sql_add_terminal_index)
    uuid2terminals = _get_uuid2terminals(con, cur)
    sql_add_rec = """\
    INSERT INTO {tbl}
    (id, uuid, terminal_uuid)
    VALUES (%s, %s, %s) 
    """.format(tbl=conf.TERMINALS_TBL)
    for n, row in enumerate(uuid2terminals, 1):
        uuid, terminal_uuid = row
        cur.execute(sql_add_rec, (n, uuid, terminal_uuid))
    con.commit()
    print("Made terminals table")

def make_root_seg_sequence2uuid(con, cur):
    """
    Get from root_seg_uuid, and sequence_id to uuid.

    Note -- cannot assume only terminal levels have sequence_ids so need to
    filter to terminal level.
    """
    _drop_tbl(con, cur, tbl=conf.SEQ2UUID_TBL)
    sql_make_tbl = """\
    CREATE TABLE {tbl} (
      root_seg_uuid text,
      sequence_id integer,
      uuid text NOT NULL,
      PRIMARY KEY (root_seg_uuid, sequence_id)
    )
    """.format(tbl=conf.SEQ2UUID_TBL)
    cur.execute(sql_make_tbl)
    sql_add_root_seg_index = """\
    CREATE INDEX ON {tbl} (root_seg_uuid, sequence_id)
    """.format(tbl=conf.SEQ2UUID_TBL)
    cur.execute(sql_add_root_seg_index)
    sql_add_uuid_index = """\
    CREATE INDEX ON {tbl} (uuid)
    """.format(tbl=conf.SEQ2UUID_TBL)
    cur.execute(sql_add_uuid_index)
    sql_populate_tbl = """\
    INSERT INTO {dest_tbl}
    SELECT root_seg_uuid, sequence_id, MIN(uuid)  /* should only be one */
    FROM {src_tbl}
    WHERE sequence_id IS NOT NULL
    AND level = %s
    GROUP BY root_seg_uuid, sequence_id
    """.format(src_tbl=conf.SEGDETS_TBL, dest_tbl=conf.SEQ2UUID_TBL)
    cur.execute(sql_populate_tbl, (conf.TERMINAL_LEVEL, ))
    con.commit()
    print("Made seq2uuid table")

def _get_useful_seg_uuids(cur, level=None):
    """
    Only get seg uuids for segments that are descended from incident routes
    (excluding side roads).
    """
    level_filter_clause = ("WHERE level = {}".format(level) if level is not None
        else "")
    sql_useful_seg_uuids = """\
    SELECT DISTINCT uuid
    FROM {tbl}
    {level_filter_clause}
    """.format(tbl=conf.SEGDETS_TBL, level_filter_clause=level_filter_clause)
    cur.execute(sql_useful_seg_uuids)
    return [x[0] for x in cur.fetchall()]

def _get_useful_seg_uuid_clause(cur, level=None):
    """Get SQL clause ready to filter by uuids"""
    useful_seg_uuids = _get_useful_seg_uuids(cur, level)
    filter_clause = "('" + "','".join(useful_seg_uuids) + "')"
    return filter_clause

def make_traffic_unnormalised_tbl(con, cur):
    """
    Make filtered version of segment_traffic_data so only includes segments of
    interest - namely segments in incident response root routes. Still including
    a mix of levels as supplied by dynamic polling. Not yet normalised down to
    terminal level.
    """
    _drop_tbl(con, cur, tbl=conf.TRAFFIC_UNNORMALISED_TBL)
    filter_clause = _get_useful_seg_uuid_clause(cur)
    sql_make_tbl = """\
    SELECT
      uuid,
      src_level,
      date,
      {get_date_group_id},
      duration,
      duration_in_traffic,
      unadjusted_delay_pct
    INTO {dest_tbl}
    FROM (
      SELECT
        uuid,
          level AS
        src_level,
        {raw_date_fields_from_date},
        {raw_duration_diff_fields_from_durs}
      FROM {src_tbl} AS src
      INNER JOIN {segdets}
      USING(uuid)
      WHERE uuid IN {filter_clause}
    ) AS raw_date_duration_diff_bits
    """.format(src_tbl=conf.SEGMENT_TRAFFIC_TBL,
        dest_tbl=conf.TRAFFIC_UNNORMALISED_TBL,
        filter_clause=filter_clause, segdets=conf.SEGDETS_TBL,
        get_date_group_id=date_group_id_field_from_raw_date_fields,
        raw_date_fields_from_date=raw_date_fields_from_date,
        raw_duration_diff_fields_from_durs=raw_duration_diff_fields_from_durs)
    cur.execute(sql_make_tbl)
    sql_add_uuid_index = """\
    CREATE INDEX ON {tbl} (uuid)
    """.format(tbl=conf.TRAFFIC_UNNORMALISED_TBL)
    cur.execute(sql_add_uuid_index)
    sql_add_date_group_id_index = """\
    CREATE INDEX ON {tbl} (date_group_id)
    """.format(tbl=conf.TRAFFIC_UNNORMALISED_TBL)
    cur.execute(sql_add_date_group_id_index)
    con.commit()
    print("Made unnormalised traffic table")

def make_traffic_normalised_tbl(con, cur):
    """
    Make normalised version of (filtered) traffic_data so everything normalised
    to terminal level e.g. if we have a level 0 we apply all its values to its
    level 2 descendants.

    Given the version of dynamic polling used currently keeps on polling
    ancestors until they have no descendants being polled (apart from level 0s
    which are polled even if no descendants being polled) we will have
    duplicates for terminal levels - the propagated version and the actual.

    One approach - splay everything and pick version of terminal node rows with
    highest src_level e.g.

    One level 2 segment experiencea possible congestion so we drill down under
    all bad segments until we get to the lowest level. Note - we leep polling
    ancestors until no bad descendants.

    0 (bad) ->
    0 (keep polling)
    1a (bad), 1b (good) ->
    0, 1a, and 1b (keep polling)
    2a1 (bad), 2a2 (good)

    so we collect data like:

    level      uuid     duration_diff
    ---------------------------------
    0          0        5
    1          1a       20
    1          1b       10
    2          2a1      21
    2          2a2      19

    which normalises to:

    src_level  uuid     duration_diff    comment
    ------------------------------------------------------
    0          2a1      5                propagated from level 0
    0          2a2      5                "
    0          2b1      5                "
    0          2b2      5                "
    1          2a1      20               propagated from level 1
    1          2a2      20               "
    1          2b1      10               "
    1          2b2      10               "
    2          2a1      21               actual value polled
    2          2a2      19               "

    For this we need
    SELECT DISTINCT ON (uuid) <fields list> ORDER BY src_level DESC
    so the version of the 3 values for 2a1 and 2a2 chosen is that with the
    highest src_level i.e. 2 i.e. 15 and 16. For 2b1 and 2b2 the highest
    src_level is 1 so it's 11 in both cases (propagated from level 1 uuid 1b).

    uuid  duration_diff  comment
    -----------------------------------
    2a1   21             highest src_level is 2
    2a2   19             "
    2b1   10             highest src_level is 1
    2b2   10

    QED

    Note - because of the nested and splaying nature of the data we will have
    far more lower-level segments called when we are drilling down BUT because
    the highest-level segments are polled all the time, and drilling down in
    rare, we might get roughly similar numbers for each level overall e.g. a
    ratio of 1:3:3 for levels 0:1:2. We would not expect 1:24:240 or such like.
    """
    _drop_tbl(con, cur, tbl=conf.TRAFFIC_NORMALISED_TBL)
    sql_make_tbl = """\
    SELECT
    uuid,
    polled_uuid,
    src_level,
    len_metres,
    date,
    date_group_id,
    src_duration,
    src_duration_in_traffic,
    unadjusted_delay_pct
    INTO {dest_tbl}
    FROM (
      SELECT DISTINCT ON (terminal_uuid, date)
        terminal_uuid AS
      uuid,
        uuid AS
      polled_uuid,
      date,
      src_level,
      date_group_id,
        duration AS
      src_duration,
        duration_in_traffic AS
      src_duration_in_traffic,
      unadjusted_delay_pct
      FROM {src_tbl}
      INNER JOIN {splay_tbl}
      USING(uuid)
      ORDER BY uuid ASC, date ASC, src_level DESC
    ) AS splayed_data
    INNER JOIN {segdets}
    USING(uuid)
    ORDER BY uuid ASC, date ASC
    """.format(src_tbl=conf.TRAFFIC_UNNORMALISED_TBL,
        splay_tbl=conf.TERMINALS_TBL, dest_tbl=conf.TRAFFIC_NORMALISED_TBL,
        segdets=conf.SEGDETS_TBL)
    cur.execute(sql_make_tbl)
    sql_add_uuid_idx = """\
    CREATE INDEX ON {tbl} (uuid)
    """.format(tbl=conf.TRAFFIC_NORMALISED_TBL)
    cur.execute(sql_add_uuid_idx)
    sql_add_date_group_id_index = """\
    CREATE INDEX ON {tbl} (date_group_id)
    """.format(tbl=conf.TRAFFIC_NORMALISED_TBL)
    cur.execute(sql_add_date_group_id_index)
    con.commit()
    print("Made normalised traffic table")

def make_date_groups_tbl(con, cur):
    """Make lookup table for date groups"""
    sql_drop_tbl = """\
    DROP TABLE IF EXISTS {tbl}
    """.format(tbl=conf.DATE_GROUPS_TBL)
    cur.execute(sql_drop_tbl)
    con.commit()
    sql_make_tbl = """\
    CREATE TABLE {tbl}
    (
      date_group_id smallint,
      date_group_lbl text NOT NULL,
      PRIMARY KEY (date_group_id)
    )
    """.format(tbl=conf.DATE_GROUPS_TBL)
    cur.execute(sql_make_tbl)
    subclauses = ["({}, '{}')".format(*tup) for tup in conf.DATE_GROUPS]
    values_clause = ",\n".join(subclauses)
    sql_populate = """\
    INSERT INTO {tbl}
    (date_group_id, date_group_lbl) VALUES
    {values}
    """.format(tbl=conf.DATE_GROUPS_TBL, values=values_clause)
    cur.execute(sql_populate)
    con.commit()
    print("Made date groups table")

def make_top_level_date_group_expecteds_tbl(con, cur):
    """
    Make table mapping top-level (level 0) segments to expected duration_diffs.
    Using top-level only because the data for lower levels is heavily biased by
    dynamic polling. The smallest level segments are only polled when there is
    a known problem. So the average duration_diff will be high and will be hard
    to exceed even when there is an incident.

    Currently the mean duration_diff across a historical data set. In future will
    be able to construct table for all level segments by polling ahead (future)
    e.g. what is expected duration_diff in one, two, three, four weeks etc.
    """
    _drop_tbl(con, cur, tbl=conf.ROOT_EXPECTEDS_TBL)
    ## Cartesian product so every option is guaranteed to be covered. Will be Left-Joined to actual data so we can fill in any gaps
    sql_comprehensive_keys = """\
    (SELECT uuid 
      FROM {segdets}
      WHERE level = 0
    ) AS root_segs
    CROSS JOIN
    (SELECT
    date_group_id,
    date_group_lbl
    FROM {date_groups}
    ) AS date_group_dets
    """.format(segdets=conf.SEGDETS_TBL, date_groups=conf.DATE_GROUPS_TBL)
    sql_raw_date_duration_diff_bits = """\
    SELECT
      uuid,
      {raw_date_fields_from_date},
      {raw_duration_diff_fields_from_durs},
      level
    FROM {traffic_unnormalised} INNER JOIN {segdets} USING(uuid)
    WHERE level = 0
    """.format(traffic_unnormalised=conf.TRAFFIC_UNNORMALISED_TBL,
        segdets=conf.SEGDETS_TBL,
        raw_date_fields_from_date=raw_date_fields_from_date,
        raw_duration_diff_fields_from_durs=raw_duration_diff_fields_from_durs)
    sql_make_tbl = """\
    SELECT

    uuid,
    date_group_id,
    avg_delay_pct
    INTO {dest_tbl}

    FROM (

      {sql_comprehensive_keys} 

    ) AS
    comprehensive_keys

    LEFT JOIN

    (SELECT

      uuid,
      date_group_id,
        quantile(unadjusted_delay_pct, 0.5) AS  /* median as per https://wiki.postgresql.org/wiki/Aggregate_Median - must install pgxn (and dev version of server) etc as per https://github.com/tvondra/quantile */
      avg_delay_pct

      FROM (
        SELECT
          uuid,
          date,
          {get_date_group_id},
          duration,
          duration_in_traffic,
          unadjusted_delay_pct
          FROM (

            {sql_raw_date_duration_diff_bits}

          ) AS raw_date_duration_diff_bits

      ) AS
      dates_grouped
      GROUP BY uuid, date_group_id

    ) AS
    populated

    USING(uuid, date_group_id)
    ORDER BY comprehensive_keys.uuid, comprehensive_keys.date_group_id
    """.format(dest_tbl=conf.ROOT_EXPECTEDS_TBL,
        sql_comprehensive_keys=sql_comprehensive_keys,
        sql_raw_date_duration_diff_bits=sql_raw_date_duration_diff_bits,
        get_date_group_id=date_group_id_field_from_raw_date_fields)
    cur.execute(sql_make_tbl)
    con.commit()
    print("Made top-level expecteds table ({})".format(conf.ROOT_EXPECTEDS_TBL))

def make_terminal_uuid_date_group_expecteds_tbl(con, cur):
    """
    Make table that has an expected duration diff (either delayed or quicker
    than average) for a given segment uuid (terminal segs only) and date group
    (e.g. weekday peak rush hour).

    In future will be based on polling for future dates e.g. Next Friday at
    rush hour (, and the Friday after etc and averaging).
    """
    _drop_tbl(con, cur, tbl=conf.EXPECTEDS_TBL)
    make_top_level_date_group_expecteds_tbl(con, cur)
    sql_make_tbl = """\
    SELECT terminal_uuid AS
    uuid,
    date_group_id,
    avg_delay_pct
    INTO {dest_tbl}
    FROM {root_expecteds}
    INNER JOIN
    {terminals}
    USING(uuid)
    """.format(dest_tbl=conf.EXPECTEDS_TBL,
        root_expecteds=conf.ROOT_EXPECTEDS_TBL, terminals=conf.TERMINALS_TBL)
    cur.execute(sql_make_tbl)
    con.commit()
    print("Made terminal date group expecteds")

def make_traffic_final_tbl(con, cur):
    """
    Make table linking traffic data (normalised to terminal segs), and expected
    duration diff data, to enable identification of incidents.

    Decided against calculating pct increase in pct as 0.5 -> 20% = 4000% increase.
    Instead using absolute size of difference in pct e.g. 19.5 point increase.

    Calculation for increase in duration_diff percentage = actual - expected

    e.g. actual duration diff pct = 15% but only expected 10%: 15-10 i.e.
    5 point increase

    e.g. actual duration diff pct = 10% but only expected -5% (i.e. expect
    faster than usual)
    10 - -5 = 10+5 i.e. 15
    """
    _drop_tbl(con, cur, tbl=conf.TRAFFIC_FINAL_TBL)
    sql_make_tbl = """\
    SELECT
      100*(
        (src_observed_duration - src_expected_duration)/
        CAST(src_expected_duration AS Float)
      ) AS
    adjusted_delay_pct,
    qry_tbl_source.*
    INTO {traffic_final}
    FROM (
      SELECT 
        uuid,
        polled_uuid,
        src_level,
        len_metres,
        date,
          src_duration + ((avg_delay_pct/100)*src_duration) AS
        src_expected_duration,  /* src_duration adjusted according to average % increase for that segment at that time of day e.g. peak hour */
        unadjusted_delay_pct,  /* actual difference between no-traffic (speed-limit obeying) duration and the actual duration in traffic right now */
        avg_delay_pct,  /* median duration difference for this segment for this time of day e.g. weekday peak rush hour */
        date_group_id,
        date_group_lbl,
        src_duration,
          src_duration_in_traffic AS
        src_observed_duration
        FROM (
          SELECT
          {traffic_normalised}.*,
          avg_delay_pct,
          date_group_lbl
          FROM
          {traffic_normalised}
          INNER JOIN
          {expecteds}
          USING(uuid, date_group_id)
          INNER JOIN
          {date_groups}
          USING(date_group_id)
        ) AS data_with_expecteds_and_date_groups
    ) AS qry_tbl_source
    """.format(traffic_final=conf.TRAFFIC_FINAL_TBL,
        traffic_normalised=conf.TRAFFIC_NORMALISED_TBL,
        expecteds=conf.EXPECTEDS_TBL, date_groups=conf.DATE_GROUPS_TBL)
    cur.execute(sql_make_tbl)
    con.commit()
    sql_add_uuid_idx = """\
    CREATE INDEX ON {tbl} (uuid)
    """.format(tbl=conf.TRAFFIC_FINAL_TBL)
    cur.execute(sql_add_uuid_idx)
    sql_add_date_index = """\
    CREATE INDEX ON {tbl} (date)
    """.format(tbl=conf.TRAFFIC_FINAL_TBL)
    cur.execute(sql_add_date_index)
    con.commit()
    print("Made final traffic table")

def make_factors_tbls(con, cur, cur2, throttle=False, hits_only=False,
        audit=False):
    """
    throttle -- speed everything up during test phase by date limiting and
    result limiting main source query

    hits_only -- make it easier during testing to focus on interesting
    calculations by only writing those records to the table.
    """
    if audit and not throttle:
        print("Are you crazy! No auditing unless throttling!")
        return
    if throttle:
        print("*"*100 + "\nTake date and LIMIT limits off\n" + "*"*100)
        counter_cycle = 250
    else:
        counter_cycle = 1000
    _drop_tbl(con, cur, tbl=conf.FACTORS_ROOT_DATE_TBL)
    ## factors
    sql_make_factors_tbl = """\
    CREATE TABLE {factors_root_date} (
      id SERIAL,
      root_seg_uuid text NOT NULL,
      date timestamp with time zone NOT NULL,
      edges json,
      root_seg_data json,
      segs_data json,
      unique(root_seg_uuid, date),
      PRIMARY KEY (id)
    )
    """.format(factors_root_date=conf.FACTORS_ROOT_DATE_TBL)
    cur.execute(sql_make_factors_tbl)
    ## edges
    _drop_tbl(con, cur, tbl=conf.EDGES_TBL)
    sql_make_edges_tbl = """\
    CREATE TABLE {edges} (
      id SERIAL,
      root_seg_uuid text NOT NULL,
      uuid text NOT NULL,
      date timestamp with time zone NOT NULL,
      is_edge boolean,
      unique(root_seg_uuid, uuid, date),
      PRIMARY KEY (id)
    )
    """.format(edges=conf.EDGES_TBL)
    cur.execute(sql_make_edges_tbl)
    con.commit()
    if throttle:
        where = "WHERE date > '20170101' AND date < '{}'".format('20170109')
        limit = "LIMIT {limit_number} OFFSET {limit_offset}".format(
            limit_number=2000, limit_offset=1500)
    else:
        where = ''
        limit = ''
    sql_data = """\
    SELECT date, root_seg_uuid
    FROM {traffic_final}
    INNER JOIN
    {segdets}
    USING(uuid)
    {where}
    GROUP BY date, root_seg_uuid
    ORDER BY date, root_seg_uuid
    {limit}
    """.format(traffic_final=conf.TRAFFIC_FINAL_TBL, segdets=conf.SEGDETS_TBL,
        where=where, limit=limit)
    cur.execute(sql_data)
    print("Root segment/date rows to process: {:,}".format(cur.rowcount))
    i = 1
    while True:
        rows = cur.fetchmany(500)
        if not rows:
            break
        for row in rows:
            factors._process_factor_data(con, cur2, row['date'],
                row['root_seg_uuid'], hits_only=hits_only, audit=audit)
            if i % counter_cycle == 0:
                print("Processed {:,} rows".format(i))
            i += 1
    ## factors indexing
    sql_add_factors_date_index = """\
    CREATE INDEX ON {factors_root_date} (date)
    """.format(factors_root_date=conf.FACTORS_ROOT_DATE_TBL)
    cur.execute(sql_add_factors_date_index)
    sql_add_factors_root_seg_uuid_index = """\
    CREATE INDEX ON {factors_root_date} (root_seg_uuid)
    """.format(factors_root_date=conf.FACTORS_ROOT_DATE_TBL)
    cur.execute(sql_add_factors_root_seg_uuid_index)
    ## edges indexing
    sql_add_edges_date_index = """\
    CREATE INDEX ON {edges} (date)
    """.format(edges=conf.EDGES_TBL)
    cur.execute(sql_add_edges_date_index)
    sql_add_edges_root_seg_uuid_index = """\
    CREATE INDEX ON {edges} (root_seg_uuid)
    """.format(edges=conf.EDGES_TBL)
    cur.execute(sql_add_edges_root_seg_uuid_index)
    sql_add_edges_uuid_index = """\
    CREATE INDEX ON {edges} (uuid)
    """.format(edges=conf.EDGES_TBL)
    cur.execute(sql_add_edges_uuid_index)
    con.commit()
    print("Made factors by root seg and date table and edges table")

def _make_edge_rate_tbl(con, cur, grouping_fldname, tblname):
    _drop_tbl(con, cur, tbl=tblname)
    sql_make_tbl = """\
    SELECT
    {grouping_fldname},
      AVG(monthly_edge_tot) AS
    avg_monthly_edge_tot
    INTO {seg_edge_rates}
    FROM (
      SELECT
      {grouping_fldname},
      month,
        SUM(edge_sum_me) AS
      monthly_edge_tot
      FROM (
        SELECT
        {grouping_fldname},
          EXTRACT(MONTH FROM date) AS
        month,
          CASE is_edge
            WHEN True THEN 1
            ELSE 0
          END
          AS
        edge_sum_me
        FROM {edges}
      ) AS
      month_recs
      GROUP BY {grouping_fldname}, month
    ) AS
    month_edge_tots
    GROUP BY {grouping_fldname}
    """.format(grouping_fldname=grouping_fldname,
        seg_edge_rates=tblname, edges=conf.EDGES_TBL)
    cur.execute(sql_make_tbl)
    sql_add_idx = """\
    CREATE INDEX ON {tblname} ({grouping_fldname})
    """.format(tblname=tblname, grouping_fldname=grouping_fldname)
    cur.execute(sql_add_idx)
    con.commit()
    print("Made monthly edge rate table ({})".format(tblname))

def make_seg_edge_rate_tbl(con, cur):
    _drop_tbl(con, cur, tbl=conf.SEG_EDGE_RATES_TBL)
    _make_edge_rate_tbl(con, cur, grouping_fldname='uuid',
        tblname=conf.SEG_EDGE_RATES_TBL)

def make_root_seg_edge_rate_tbl(con, cur):
    _drop_tbl(con, cur, tbl=conf.ROOT_SEG_EDGE_RATES_TBL)
    _make_edge_rate_tbl(con, cur, grouping_fldname='root_seg_uuid',
        tblname=conf.ROOT_SEG_EDGE_RATES_TBL)

def _get_rem():
    """
    Running via localhost using ssh forwarding so can check database via
    PGadminIII

    ssh -L 5432:10.100.100.11:5432 root@ambari01
    """
    pwd_rem = not4git.access_rem  #getpass.getpass('Enter remote postgres password: ')
    con_rem = pg.connect("dbname='transport_db' "
        "user='transport_admin' "
        "host='localhost' "
        "port=5432 "
        "password='{}' "
        "connect_timeout=5".format(pwd_rem))
    cur_rem = con_rem.cursor(cursor_factory=pg_extras.DictCursor)
    return con_rem, cur_rem

def _array2json(pg_array):
    return '{' + ','.join(pg_array) + '}'

def make_segments_tbl(cur_rem, con_local, cur_local):
    ## make table
    _drop_tbl(con_local, cur_local, tbl=conf.SEGS_TBL)
    sql_make_tbl = """\
    CREATE TABLE {segs}
    (
      uuid text NOT NULL,
      heading text NOT NULL,
      data json NOT NULL,
      geom geometry NOT NULL,
      polyline_hash text NOT NULL,
      tags text[],
      active boolean NOT NULL DEFAULT true,
      route_id integer NOT NULL,
      id serial NOT NULL,
      directions_hash text,
      parent_uuid text,
      sequence_id integer
    )
    """.format(segs=conf.SEGS_TBL)
    cur_local.execute(sql_make_tbl)
    con_local.commit()
    ## get and insert data
    sql_insert_tpl = """\
    INSERT INTO {segs}
    (uuid, heading, data, geom, polyline_hash, tags, active, route_id, id, directions_hash, parent_uuid, sequence_id)
    VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
    """.format(segs=conf.SEGS_TBL)
    sql_get_segs_data = """\
    SELECT *
    FROM {segs}
    """.format(segs='transport.segments')
    cur_rem.execute(sql_get_segs_data)
    data = cur_rem.fetchall()  ## only small table so faster
    for row in data:
        cur_local.execute(sql_insert_tpl,
            (row['uuid'], row['heading'], json.dumps(row['data']), row['geom'],
             row['polyline_hash'], _array2json(row['tags']), row['active'],
             row['route_id'], row['id'], row['directions_hash'],
             row['parent_uuid'], row['sequence_id']))
    con_local.commit()
    print("Made segments table")

def make_routes_tbl(cur_rem, con_local, cur_local):
    ## make table
    _drop_tbl(con_local, cur_local, tbl=conf.ROUTES_TBL)
    sql_make_tbl = """\
    CREATE TABLE {routes}
    (
      route_id integer NOT NULL,
      tags text[],
      site_id integer,
      parent_id integer,
      data jsonb,
      PRIMARY KEY(route_id)
    )
    """.format(routes=conf.ROUTES_TBL)
    cur_local.execute(sql_make_tbl)
    con_local.commit()
    ## get and insert data
    sql_insert_tpl = """\
    INSERT INTO {routes}
    (route_id, tags, site_id, parent_id, data)
    VALUES(%s, %s, %s, %s, %s)
    """.format(routes=conf.ROUTES_TBL)
    sql_get_routes_data = """\
    SELECT *
    FROM {routes}
    """.format(routes='transport.routes')
    cur_rem.execute(sql_get_routes_data)
    data = cur_rem.fetchall()  ## only small table so faster
    for row in data:
        cur_local.execute(sql_insert_tpl, (row['route_id'],
            _array2json(row['tags']), row['site_id'], row['parent_id'],
            json.dumps(row['data'])))
    con_local.commit()
    print("Made routes table")

def make_segment_traffic_data_tbl(cur_rem, con_local, cur_local):
    """
    Only for uuids of segments in segments table.
    """
    ## make table
    _drop_tbl(con_local, cur_local, tbl=conf.SEGMENT_TRAFFIC_TBL)
    sql_make_tbl = """\
    CREATE TABLE {traffic}
    (
      uuid text NOT NULL,
      date timestamp with time zone NOT NULL,
      duration integer NOT NULL,
      duration_in_traffic integer NOT NULL,
      polyline_hash text NOT NULL,
      data json,
      extraction_date timestamp with time zone
    )
    """.format(traffic=conf.SEGMENT_TRAFFIC_TBL)
    cur_local.execute(sql_make_tbl)
    con_local.commit()
    ## get and insert data
    con_local.commit()
    root_route_ids = []
    for pair in conf.ROUTE_SRC_IDS:
        root_route_ids.extend(list(pair))
    uuids = sorted(seg_helpers.root_route_ids2uuids(cur_local, root_route_ids))
    where_uuids_in_routes = "WHERE uuid IN ('" + "', '".join(uuids) + "')"
    sql_get_data = """\
    SELECT *
    FROM {traffic}
    {where_uuids_in_routes}
    """.format(traffic=conf.SEGMENT_TRAFFIC_TBL,
        where_uuids_in_routes=where_uuids_in_routes)
    cur_rem.execute(sql_get_data)
    sql_insert_tpl = """\
    INSERT INTO {traffic}
    (uuid, date, duration, duration_in_traffic, polyline_hash, data, extraction_date)
    VALUES(%s, %s, %s, %s, %s, %s, %s)
    """.format(traffic=conf.SEGMENT_TRAFFIC_TBL)
    n = 1
    while True:
        row = cur_rem.fetchone()
        if not row:
            break
        cur_local.execute(sql_insert_tpl, (row['uuid'], row['date'],
            row['duration'], row['duration_in_traffic'], row['polyline_hash'],
            json.dumps(row['data']), row['extraction_date']))
        if n % 500 == 0:
            print("Added {:,} rows to {}".format(n, conf.SEGMENT_TRAFFIC_TBL))
        n += 1
    con_local.commit()
    print("Made filtered segment_traffic_data table (filtered according to "
        "conf.ROUTE_SRC_IDS)")

def make_tbls(con, cur, cur2):
    """
    Make source tables to make it quick, easy, and safe to make actual
    visualisation source data.
    """
    unused, cur_rem = _get_rem()
    '''make_segments_tbl(cur_rem, con, cur)
    make_routes_tbl(cur_rem, con, cur)
    make_segment_traffic_data_tbl(cur_rem, con, cur)
    make_reverses_tbl(con, cur)
    make_seg_dets_tbl(con, cur)'''
    make_root_seg_sequence2uuid(con, cur)
    make_uuid2terminals_tbl(con, cur)
    make_traffic_unnormalised_tbl(con, cur)
    make_traffic_normalised_tbl(con, cur)
    make_date_groups_tbl(con, cur)
    make_terminal_uuid_date_group_expecteds_tbl(con, cur)
    make_traffic_final_tbl(con, cur)
    make_factors_tbls(con, cur, cur2, throttle=False)
    make_root_seg_edge_rate_tbl(con, cur)
    make_seg_edge_rate_tbl(con, cur)
