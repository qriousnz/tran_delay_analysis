#! /usr/bin/env python3

from collections import namedtuple
from pprint import pprint as pp

import conf
import date_helpers
import report_heat_map
import report_helpers
import report_images
import report_maps
import seg_helpers

from webbrowser import open_new_tab

PeakDetails = namedtuple('PeakDetail', 'surrounding_uuids, start_dt, end_dt')

HTML_START_TPL = """\
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<title>%(title)s</title>
<script src="js/jquery-3.1.1.min.js"></script>
<script>
<!--$(document).ready(function() {
  $('.yaxis_lbl').css('height', $('.yaxis_lbl').width());
});-->
</script>
<style>
body {
  padding: 0;
  margin: 0;
  font-size: 12px;
  font-family: arial;
}
#content {
  padding: 12px;
}
#banner {
  color: white;
  font-size: 37px;
  font-weight: normal;
  padding: 12px 0 0 225px;
  margin: 0;
  background-color: #241c54;
  background-image: url("images/format/Qrious-Logo-White-Trans.png");
  background-repeat: no-repeat;
  background-size: 160px;
  background-position: 18px 50%%;
  height: 57px;
}
h2 {
  color: #241c54;
  padding: 0;
  margin: 0;
}

%(more_styles)s
</style>
</head>
<body>
<div id='banner'>Context for High-Delay Events</div>
<div id='content'>
"""

def make_report(cur, focus_uuid, focus_dt, points_breakdown, inc_start_dt=None,
        inc_identified_dt=None,time_series_img_limit=None,
        seg_series_img_limit=None):
    """
    focus_dt -- date we focus analysis on. If an incident, the focus_dt is the
    incident start date rather than the date we started the analysis from.

    inc_start_dt and inc_identified_dt are only used to identify particular
    snapshot charts as being for incident start or incident confirmed.

    might not be an incident but we are focusing on this particular date
    """
    is_inc = bool(inc_start_dt)
    print("About to make report ...")
    report_id = report_helpers.get_report_id(focus_uuid, focus_dt)
    title = "Incident Visualisation - {}".format(report_id)
    html = [
        HTML_START_TPL % {'title': title,
        'more_styles': """\
        .seg-image {
          width: 369px;
          padding: 0;
          margin: 0;
          float: 'left';
        }
        .long-term-image {
          width: 747px;
          padding: 0;
          margin: 0;
          float: 'left';
        }
        .snapshot-image {
          width: 369px;
          padding: 0;
          margin: 0;
          float: 'left';
        }
        .small {
          font-size: 7px;
          float: 'left';
        }
        .shrink {
          font-size: 5px;
          float: 'left';
        }
        #map-event-datetime {
          position: absolute;
          left:75px;
          top: 100px;
          font-weight: bold;
          font-size: 14pt;
        }
        .left-align {
          text-align: left;
        }
        .right-align {
          text-align: right;
        }
        .lbl {
          text-align: right;
          font-size: 16px;
          font-weight: bold;
        }
        th.lbl {
          padding-right: 12px;
        }
        table#metrics th, table#metrics td {
          vertical-align: top;
          padding: 6px;
        }
        table#heat-colour {
          margin-top: 20px;
        }
        table#heat {
          border-collapse: collapse;
          font-family: Arial;
          font-weight: bold;
        }
        table#heat td {
          padding: 6px 12px 6px 0;
          text-align: right;
          vertical-align: middle;
          width: 50px;
        }
        table#heat td.incident {
          border: solid 2px black;
          width: 75px;
        }
        table#heat tr.focus-row {
          border-top: solid 4px #c5c5c5;
          border-bottom: solid 4px #c5c5c5;
        }
        table#heat td.comment {
          font-size: 13px;
          text-align: left;
          font-weight: normal;
          vertical-align: top;
        }
        table#heat td.row-lbl {
          width: 130px;
        }
        table#heat td .date {
          font-size: 20px;
        }
        table#heat td .time {
          font-size: 10px;
        }
        table#heat td .incident-lbl {
          font-size: 13px;
          font-weight: bold;
        }
        table#colour-map {
          margin-left: 30px;
        }
        table#colour-map th#colour-map-col-heading {
          font-size: 24px;
        }
        table td#right-cell {
          vertical-align: top;
        }
        .bright-green {
          background-color: #5dd35d;
        }
        .green {
          background-color: #28a624;
        }
        .light-orange {
          background-color: #ffd92a;
        }
        .mid-orange {
          background-color: #ffc02a;
        }
        .red-orange {
          background-color: #ffa502;
        }
        .light-red {
          background-color: #ff7102;
        }
        .bright-red {
          background-color: #ff3d2a;
        }
        .lurid-red {
          background-color: #ff0000;
        }
        .white {
          background-color: white;
        }
        .heatmap-text {
          text-align: right;
          font-size: 13px;
          padding-right: 13px;
        }
        .heatmap-colour {
          width: 12px;
        }
        .symbol {
          font-size: 20px;
        }
        .large-symbol {
          font-size: 30px;
        }
        table#heat td.xaxis_lbl, table#heat td.yaxis_lbl {
          font-size: 13px;
          font-weight: bold;
        }
        table#heat td.xaxis_lbl {
          text-align: center;
        }
        table#heat td.yaxis {
          width: 25px;
          padding: 0;
        }
        table#heat td div.yaxis_lbl {
          margin-right: -100px;

          -ms-transform: rotate(-90deg);
          -moz-transform: rotate(-90deg);
          -webkit-transform: rotate(-90deg);
          transform: rotate(-90deg);

          -ms-transform-origin: left top 0;
          -moz-transform-origin: left top 0;
          -webkit-transform-origin: left top 0;
          transform-origin: left top 0;
        }
        """},
    ]
    ## init
    print("    Getting uuid details ...", end=' ')
    uuid_dets, reverse_uuid_dets = seg_helpers.surrounding_uuids_dets(
         cur, seg_uuid=focus_uuid, is_inc=is_inc)
    print("DONE")
    std_dates_clause = date_helpers.get_surrounding_dates_clause(focus_dt)
    uuids = [x['uuid'] for x in uuid_dets]
    uuids_both_directions = (uuids + [x['uuid'] for x in reverse_uuid_dets])
    print("    Getting y-axis bounds ...", end=' ')
    ymin, ymax = report_helpers._get_ybounds(cur, uuids_both_directions,
        std_dates_clause)
    print("DONE")

    ## segment display
    print("Displaying segs on map ...", end=' ')
    src_url = report_maps.map_segs(cur, report_id, focus_uuid, uuid_dets,
        reverse_uuid_dets, display=False)
    print("DONE")
    datetime_lbl = (focus_dt.strftime("%-I:%M%p %a %d %b %Y").replace('PM', 'pm')
        .replace('AM', 'am'))
    html.append("<div id='map-event-datetime'>{}</div>".format(datetime_lbl))
    html.append("<iframe src='{src}' width=1150px height=200px></iframe>"
        .format(src=src_url))

    ## points_breakdown
    if points_breakdown:
        html.append("<h2>Points breakdown</h2>")
        html.append("<table id='metrics'>\n<thead>"
            "\n<tr>"
            "\n  <th class='left-align'>Label</th>"
            "<th class='right-align'>Raw measure</th>"
            "<th class='right-align'>Score</th>"
            "\n</tr>\n</thead>\n<tbody>")
        for points_details in points_breakdown:
            html.append("<tr>"
                "\n  <td>{}</td>"
                "<td class='right-align'>{}</td>"
                "<td class='right-align'>{}</td>"
                .format(*points_details))
        html.append("</tbody>\n</table>")

    ## heat map
    uuids_clause = seg_helpers.uuids2clause([d['uuid'] for d in uuid_dets])
    heatmap_items = report_heat_map.get_expected_congestion_chart(cur, focus_dt,
        std_dates_clause, uuid_dets, uuids, uuids_clause, is_inc)
    html.extend(heatmap_items)

    ## auditing details - segment and date details
    html.append("<h2>SQL for auditing results on original segment traffic table"
        "</h2>")
    segs2uuids = []
    for details in sorted(uuid_dets, key=lambda d: d['sequence_id']):
        seg_heading = seg_helpers.get_seg_heading(details['heading'],
            details['sequence_id'])
        segs2uuids.append((seg_heading, details['uuid']))
    seg_lbl_case = ("CASE segs.uuid<br>&nbsp;&nbsp;&nbsp;&nbsp;"
        + "<br>&nbsp;&nbsp;&nbsp;&nbsp;".join("WHEN '{}' THEN '{}'"
                .format(uuid, seg_heading)
            for seg_heading, uuid in segs2uuids)
        + "<br>&nbsp;&nbsp;END")
    sql_audit = """\
    SELECT<br>segs.uuid,<br>&nbsp;&nbsp;
      {seg_lbl_case}<br>&nbsp;&nbsp;AS<br>
    seg_lbl,<br>&nbsp;&nbsp;
      to_char(date::Time, 'FMHH12:MI:SS AM') AS<br>
    time_day,<br>&nbsp;&nbsp;
      round(
        100*((duration_in_traffic-duration)/CAST(duration AS Float))::numeric,
      2) AS<br>
    unadjusted_delay_pct,
    duration,<br>
    duration_in_traffic,<br>
    date<br>
    FROM {segment_traffic_data}<br>
    INNER JOIN<br>
    {segs} AS segs<br>
    USING(uuid)<br>
    WHERE {std_dates_clause}<br>
    AND {uuids_clause}<br>
    ORDER BY seg_lbl, date
    """.format(seg_lbl_case=seg_lbl_case, segs=conf.SEGS_TBL,
        segment_traffic_data=conf.SEGMENT_TRAFFIC_TBL,
        std_dates_clause=std_dates_clause, uuids_clause=uuids_clause)
    html.append("<p class='shrink'>{}</p>".format(sql_audit))

    ## snapshot charts
    html.append("<table>\n<tbody>")
    html.append("""\
    <tr>
      <td colspan="2">
        <h2>Block-release edge? Ripple effects?</h2>
        <p>See any patterns where left-most segments are delayed while
        right-most are not (cars blocked to left of incident, then released to
        speed off)?</p>
        <p>Any patterns where congestion ripples leftwards from incident as time
        passes?</p>
      </td>
    </tr>
    """)
    snapshot_items = report_images.make_seg_snapshot_charts(cur, report_id,
        focus_uuid, focus_dt, inc_start_dt, inc_identified_dt, std_dates_clause,
        uuid_dets, seg_series_img_limit, ymax)
    html.extend(snapshot_items)
    html.append("</tbody>\n</table>")   

    ## segment traffic data displayed for incident segment
    inc_uuid_dets = seg_helpers.get_inc_uuid_dets(focus_uuid, uuid_dets)
    html.append("<table>\n<tbody>")

    ## a) larger context charts
    html.append("""\
    <tr>
      <td colspan="2">
        <h2>Lots of surrounding incidents on same segment?</h2>
        <p>Too many incidents to be actual crashes, road slips etc? Only random
        holiday congestion?</p>
      </td>
    </tr>
    """)
    long_term_img, time_series_img_limit = report_images.make_long_term_chart(
        cur, focus_uuid, focus_dt, inc_uuid_dets, report_id,
        time_series_img_limit, width=25, height=2.5, days_either_side=7,
        include_inc_zone=True, is_inc=is_inc)
    html.append(long_term_img)
    all_dates_img, time_series_img_limit = report_images.make_long_term_chart(
        cur, focus_uuid, focus_dt, inc_uuid_dets, report_id,
        time_series_img_limit, width=75, height=7.5, days_either_side=None,
        include_inc_zone=False, is_inc=is_inc)
    html.append(all_dates_img)

    ## b) time series charts per segment
    html.append("<tr><td><h2>{}</h2></td> <td><h2>{}</h2></td></tr>"
        .format("Surrounding segments", "Surrounding segments (other side)"))
    img_cols = report_images.make_std_seg_charts(cur, report_id,
        focus_uuid, focus_dt, std_dates_clause, uuid_dets, reverse_uuid_dets,
        ymin, ymax, time_series_img_limit, is_inc)
    n_items = 1 + 2*conf.SEGS_EITHER_SIDE
    for i in range(n_items):
        items = []
        for img_col in img_cols:
            try:
                item = img_col[i]
            except IndexError:
                item = ""
            items.append(item)
        html.append("<tr>\n  {}\n  {}\n</tr>".format(*items))
    html.append("</tbody>\n</table>")

    ## display
    content = "\n".join(html)
    fpath = ("/home/gps/Documents/transport/delay_analysis/reports/"
        "incidents_{}.html".format(report_id))
    with open(fpath, 'w') as f:
        f.write(content)
    open_new_tab("file://{}".format(fpath))
