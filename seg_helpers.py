#! /usr/bin/env python3

import conf

from pprint import pprint as pp
from statistics import mean, mode

def get_inc_uuid_dets(inc_uuid, uuid_dets):
    inc_uuid_dets = [x for x in uuid_dets if x['uuid'] == inc_uuid][0]
    return inc_uuid_dets

def get_seg_heading(heading, seq):
    seg_heading = "{heading}{seq}".format(heading=heading, seq=seq)
    return seg_heading

def get_main_heading(uuid_dets):
    """Get mode of main part of heading data"""
    headings = [x['heading'][0] for x in uuid_dets]
    mode_heading = mode(headings)
    heading2main = {'E': conf.E, 'W': conf.W, 'N': conf.N, 'S': conf.S}
    main_heading = heading2main[mode_heading]
    return main_heading

def coords2central_coord(origin_lat, origin_lng, dest_lat, dest_lng):
    central_coord = (mean((origin_lat, dest_lat)),
        mean((origin_lng, dest_lng)))  ## yep - won't work everywhere on globe
    return central_coord

def get_central_coord(uuid_dets):
    origin_lat, origin_lng = uuid_dets['origin_coords']
    dest_lat, dest_lng = uuid_dets['dest_coords']
    central_coord = coords2central_coord(origin_lat, origin_lng, dest_lat,
        dest_lng)
    return central_coord

def get_seq2uuid(cur, root_seg_uuid):
    """Get dictionary mapping sequence to uuid for given root_seg_uuid"""
    sql_seq2uuid_in_root_seg = """\
    SELECT sequence_id, uuid
    FROM {tbl}
    WHERE root_seg_uuid = %s
    """.format(tbl=conf.SEQ2UUID_TBL)
    cur.execute(sql_seq2uuid_in_root_seg, (root_seg_uuid, ))
    seq2uuid = dict(cur.fetchall())
    return seq2uuid

def get_reverse_root_seg_uuid(cur, root_seg_uuid):
    """
    Get root segment uuid (not to be confused with the root route_id).
    """
    sql_get_reverse = """\
    SELECT reverse_root_seg_uuid
    FROM {tbl}
    WHERE root_seg_uuid = %s
    """.format(tbl=conf.REVERSES_TBL)
    cur.execute(sql_get_reverse, (root_seg_uuid, ))
    reverse_root_uuid = cur.fetchone()[0]
    return reverse_root_uuid

def uuids2clause(uuids):
    """Get uuids clause ready to add after WHERE"""
    uuids_clause = "uuid IN('" + "','".join(uuids) + "')"
    return uuids_clause

def uuids_report(cur, uuids, lbl):
    print(lbl)
    uuids_clause = uuids2clause(uuids)
    sql_segdets = """\
    SELECT *
    FROM {segdets}
    WHERE uuid IN ({uuids_clause})
    """.format(segdets=conf.SEGDETS_TBL, uuids_clause=uuids_clause)
    cur.execute(sql_segdets)
    data = {x['uuid']: dict(x) for x in cur.fetchall()}
    for uuid in uuids:
        details = data[uuid]
        print("{sequence:03} From {origin_address} to {dest_address} "
            "({length:.2f} metres; coords {origin_coords} to {dest_coords})"
            .format(
            sequence=details['sequence_id'],
            origin_address=details['origin_address'].strip(', New Zealand'),
            dest_address=details['dest_address'].strip(', New Zealand'),
            length=details['len_metres'],
            origin_coords=details['origin_coords'],
            dest_coords=details['dest_coords']))

def surrounding_uuids(cur, uuid, verbose=False):
    """
    verbose -- show all address ranges in order
    """
    ## get uuid details
    sql_get_dets = """\
    SELECT
    root_seg_uuid,
    sequence_id
    FROM {tbl}
    WHERE uuid = %s
    """.format(tbl=conf.SEQ2UUID_TBL)
    cur.execute(sql_get_dets, (uuid, ))
    root_seg_uuid, sequence_id = cur.fetchone()
    seq2uuid = get_seq2uuid(cur, root_seg_uuid)
    surrounding_seg_uuids = []
    ## direct neighbours
    for seq in range(sequence_id - conf.SEGS_EITHER_SIDE,
            sequence_id + conf.SEGS_EITHER_SIDE + 1):
        try:
            surrounding_seg_uuids.append(seq2uuid[seq])
        except KeyError:
            pass
    ## neighbours of reverse
    reverse_surrounding_seg_uuids = []
    reverse_root_seg_uuid = get_reverse_root_seg_uuid(cur, root_seg_uuid)
    n_descendants = len(seq2uuid)
    seqs = sorted(seq2uuid.keys())
    sequence_id_idx = seqs.index(sequence_id)
    approx_reverse_sequence_id_idx = n_descendants - 1 - sequence_id_idx  ## approx because may not be exactly same number of segs or may be small segs starting/ending routes because of rounding errors
    reverse_seq2uuid = get_seq2uuid(cur, reverse_root_seg_uuid)
    reverse_seqs = sorted(reverse_seq2uuid.keys())
    approx_reverse_seq = reverse_seqs[approx_reverse_sequence_id_idx]
    for seq in range(approx_reverse_seq - conf.SEGS_EITHER_SIDE,
            approx_reverse_seq + conf.SEGS_EITHER_SIDE + 1):
        try:
            reverse_surrounding_seg_uuids.append(reverse_seq2uuid[seq])
        except KeyError:
            pass
    if verbose:
        uuids_report(cur, uuids=surrounding_seg_uuids,
            lbl='Surrounding - same direction')
        uuids_report(cur, uuids=reverse_surrounding_seg_uuids,
            lbl='Surrounding - reverse direction')
    return surrounding_seg_uuids, reverse_surrounding_seg_uuids

def coord_from_str(coord_str):
    coord = [float(x) for x in coord_str.strip('(').strip(')').split(',')]
    return coord

def _extract_coords(data_dict):
    origin_lat, origin_lng = coord_from_str(data_dict['origin_coords'])
    dest_lat, dest_lng = coord_from_str(data_dict['dest_coords'])
    origin_coords = (origin_lat, origin_lng)
    dest_coords = (dest_lat, dest_lng)
    coords = (origin_coords, dest_coords)
    return origin_coords, dest_coords, coords

def _enrich_uuid_dets(cur, sequence, uuid, is_inc=True, reverse=False):
    """
    Enrich with the following:

    uuid
    sequence_id
    heading
    origin_coords -- (lat, lng)
    dest_coords -- (lat, lng)
    coords -- (origin_coords, dest_coords) (useful for drawing polylines etc)
    direction -- "towards (same side)"
                 "peak"
                 "away from (same side)"
                 "towards (other side)"
                 "near peak (other side)" (approx given unreliability of
                     identifying reverse of peak segment)
                 "away from (other side)"
    """
    uuid_dets = {'uuid': uuid}
    sql_segdets = """\
    SELECT *
    FROM {segdets}
    WHERE uuid = %s
    """.format(segdets=conf.SEGDETS_TBL)
    cur.execute(sql_segdets, (uuid, ))
    details = dict(cur.fetchone())
    uuid_dets['sequence_id'] = details['sequence_id']
    uuid_dets['heading'] = details['heading']
    ## coords
    origin_coords, dest_coords, coords = _extract_coords(details)
    uuid_dets['origin_coords'] = origin_coords
    uuid_dets['dest_coords'] = dest_coords
    uuid_dets['coords'] = coords
    ## direction
    middle_sequence = conf.SEGS_EITHER_SIDE + 1
    loclbl = '"incident"' if is_inc else 'location'
    if not reverse:
        if sequence < middle_sequence:
            direction = 'towards {}\n(same side)'.format(loclbl)
        elif sequence > middle_sequence:
            direction = 'away from {}\n(same side)'.format(loclbl)
        else:
            direction = '{}?'.format(loclbl)
    else:  ## can be inaccuracies in identifying reverse segments because of rounding error
        if sequence < middle_sequence-1:
            direction = 'towards {}\n(other side)'.format(loclbl)
        elif sequence > middle_sequence+1:
            direction = 'away from {}\n(other side)'.format(loclbl)
        else:
            direction = 'near {}\n(other side)'.format(loclbl)
    uuid_dets['direction'] = direction
    return uuid_dets

def surrounding_uuids_dets(cur, seg_uuid, is_inc=True):
    """
    Get rich details for surrounding uuids including coordinates, whether
    approaching peak or moving away etc.
    """
    seg_uuids, reverse_seg_uuids = surrounding_uuids(cur, uuid=seg_uuid)
    uuid_dets = []
    for sequence, uuid in enumerate(seg_uuids, 1):
        uuid_dets.append(_enrich_uuid_dets(cur, sequence, uuid, reverse=False,
            is_inc=is_inc))
    reverse_uuid_dets = []
    for sequence, reverse_uuid in enumerate(reverse_seg_uuids, 1):
        reverse_uuid_dets.append(_enrich_uuid_dets(cur, sequence, reverse_uuid,
            reverse=True, is_inc=is_inc))
    return uuid_dets, reverse_uuid_dets

def get_terminal_uuids4route_id(cur_local, root_route_id):
    """
    Relies on segdets table so can't be used to create filtered
    segment_traffic_data table before segdets made.
    """
    root_seg_uuid = _get_root_seg_uuid(cur_local, root_route_id)
    sql_data = """\
    SELECT uuid
    FROM {segdets}
    WHERE level = %s
    AND root_seg_uuid = %s
    ORDER BY uuid
    """.format(segdets=conf.SEGDETS_TBL)
    cur_local.execute(sql_data, (conf.TERMINAL_LEVEL, root_seg_uuid))
    data = cur_local.fetchall()
    uuids = [row['uuid'] for row in data]
    return uuids

def _get_uuids4root_uuid(cur_local, root_uuid):
    """
    Recurse that tree. Note - includes all levels and works off raw segments
    table.
    """
    sql_get_uuids = """\
    SELECT uuid
    FROM {segs}
    WHERE parent_uuid = %s
    """.format(segs=conf.SEGS_TBL)
    cur_local.execute(sql_get_uuids, (root_uuid, ))
    uuids = [row[0] for row in cur_local.fetchall()]
    child_uuids = []
    for uuid in uuids:
        child_uuids.extend(_get_uuids4root_uuid(cur_local, root_uuid=uuid))
    return uuids + child_uuids

def _get_root_seg_uuid(cur_local, root_route_id):
    sql_get_seg_uuid = """\
    SELECT uuid
    FROM {segs}
    WHERE route_id = %s
    """.format(segs=conf.SEGS_TBL)
    cur_local.execute(sql_get_seg_uuid, (root_route_id, ))
    root_seg_uuids = cur_local.fetchall()
    if len(root_seg_uuids) > 1:
        raise Exception("root_route_id '{}' matched more than once in segments"
            .format(root_route_id))
    root_seg_uuid = root_seg_uuids[0][0]
    return root_seg_uuid

def _get_uuids4route_id(cur_local, root_route_id):
    """
    Start with segments table and find root seg uuid. Then find all its children
    (i.e. those with it as the parent_uuid) and recursively go to end.
    """
    root_uuid = _get_root_seg_uuid(cur_local, root_route_id)
    uuids = [root_uuid, ]
    uuids.extend(_get_uuids4root_uuid(cur_local, root_uuid))
    return uuids

def root_route_ids2uuids(cur_local, root_route_ids):
    uuids = set()
    for root_route_id in root_route_ids:
        uuids.update(set(_get_uuids4route_id(cur_local, root_route_id)))
    return uuids

