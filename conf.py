#! /usr/bin/env python3

"""
TIMEZONES

* POSTGRESQL ****

HOUR takes hour from timezone-aware date as at the timezone you are set for (and as the data displays)
e.g. 2016-12-13 06... --> 6
so if timezone is set to 'NZ' then all is well (for postgresql - matplotlib needs its own setting to use Pacific/Auckland timezone)
if you set different timezone you'll see the time relative to that new timezone e.g. Asia/Shanghai
sudo vim /etc/postgresql/9.5/main/postgresql.conf
sudo /etc/init.d/postgresql start

* MATPLOTLIB ****
Because the data is time-zoned it is important to set timezone setting in
matplotlibrc to e.g. Pacific/Auckland (if that is what the data refers to).

* PYTHON
Don't use naive timestamps e.g. datetime.datetime(2016,1,1,12,10,0)
Instead use:

from pytz import timezone
nz = timezone('Pacific/Auckland')
nz.localize(datetime(2016, 12, 30, 12, 48, 10))
so that dates coming from postgresql (timezone-aware) and python are compatible.
Hint - if times are off by 13 hours ... ;-)
"""
ROUTES_TBL = 'routes'
SEGS_TBL = 'segments'
SEGMENT_TRAFFIC_TBL = 'segment_traffic_data'
TRAFFIC_UNNORMALISED_TBL = 'traffic_unnormalised'
SEGDETS_TBL = 'segdets'
TERMINALS_TBL = 'terminals'
SEQ2UUID_TBL = 'seq2uuid'
DATE_GROUPS_TBL = 'date_groups'
ROOT_EXPECTEDS_TBL = 'root_expecteds'
EXPECTEDS_TBL = 'expecteds'
REVERSES_TBL = 'reverses'
TRAFFIC_NORMALISED_TBL = 'traffic_normalised'
TRAFFIC_FINAL_TBL = 'traffic_final'
FACTORS_ROOT_DATE_TBL = 'factors_root_date'
EDGES_TBL = 'edges'
SEG_EDGE_RATES_TBL = 'seg_edge_rates'
ROOT_SEG_EDGE_RATES_TBL = 'root_seg_edge_rates'

TERMINAL_LEVEL = 2
SEGS_EITHER_SIDE = 6
HOURS_EITHER_SIDE = 3

## 271;"{IR,"CHCH Northern Motorway","Level 0","Section 2",reverse}";2;;"{"tags": ["IR", "CHCH Northern Motorway", "Level 0", "Section 2", "reverse"]}"
ROUTE_SRC_IDS = [  ## level 0 routes and corresponding reverse versions
    #(30, 82),
    #(108, 130),
    #(192, 271),
    #(298, 299),
    #(300, 301),
    (333, 366),  ## Auckland Motorway 1 southbound (and northbound)
]
MIN_INCIDENT_ROUTE_ID = 30  ## below 30 they are road transport routes

DATE_GROUPS = [
    (1, 'Week early'),
    (2, 'Week morning rush'),
    (3, 'Week middle day'),
    (4, 'Week school rush'),
    (5, 'Week peak rush'),
    (6, 'Week late rush'),
    (7, 'Week evening/night'),
    (8, 'Weekend day'),
    (9, 'Weekend night'),
]

LIGHT_GREY = '#8c8c8c'
DARK_GREY = '#474747'
RED = '#ff0000'
DUSK_GREEN = '#51a898'
MELON = '#f15950'

W = 'westbound'
E = 'eastbound'
N = 'northbound'
S = 'southbound'

LEFT_ALIGN = 'left'
RIGHT_ALIGN = 'right'

AXIS_HOUR = 'axis_hours'
AXIS_DAYS = 'axis_days'
AXIS_MONTHS = 'axis months'

## if our adjusted delay_pct is 60% and the next segment has an
## adjusted_delay_pct of 2% then we have a pct drop from 60 to 2. We are not
## interested in the absolute size of the percentage drop (some of the starting
## numbers are really huge e.g. 4,900% increase from 2--> 100 seconds) but the
## percentage drop i.e. 100*(60-2)/60  i.e. 96.7%.  
EDGE_DROP_PCT_THRESHOLD = 80  #80 ## don't be tempted to set this to amounts like 2000% even though figures this high will occur in the data e.g. when dropping from a delay increase of 1 to -20. Rely on the minimum pre-edge delay criterion to ensure we get the biggest block-release edges. 
EDGE_ABS_PRE_INCREASE = 200 #1000  ## what sort of delays are there before the alleged edge? 1000% increases? e.g. expected duration 1s -> observed 11s
QUEUE_CONGESTION_THRESHOLD_PCT = 200  ## delay % must be over 200 i.e. duration_in_traffic must be 3x usual duration

SEGS_DATA_KEY = 'segs_data'

SECS_5_MINS = 5*60
SECS_15_MINS = 15*60
SECS_30_MINS = 30*60
    
strength = 0  ## only 0 finds anything for Auckland motorway
## when lower strength, points more +ve = more like an incident, punishments more -ve less like one
## can't make it too realistic or else we don't get any results at all.

if strength < 2:
    print("*"*100 + "\n{:^}\n"
        .format('Restore thresholds to more rigorous levels')
        + "*"*100)
if strength == 0:
    MIN_INCIDENT_POINTS = -10000
    DELAY_DROP_SCALAR = 4
    MIN_EDGE_STABILITY = SECS_5_MINS
    LL_HIGHER_EDGE_STABILITY = SECS_15_MINS
    NORMAL_EDGE_STABILITY_POINTS = 100
    HIGH_EDGE_STABILITY_POINTS = 150
    LL_NORMAL_QUEUE_LEN = 50
    LL_HIGHER_QUEUE_LEN = 100
    NORMAL_QUEUE_LEN_POINTS = 200
    HIGH_QUEUE_LEN_POINTS = 100
    DECONGESTION_DELAY_PUNISHMENT = -0.05
    FREE_FLOWING_DELAY_PUNISHMENT = -0.05
    AVG_MONTHLY_EDGES_PUNISHMENT = -1
    PRECEDING_CONGESTION_DIVISOR = 200
elif strength == 1:
    MIN_INCIDENT_POINTS = 50
    DELAY_DROP_SCALAR = 2
    MIN_EDGE_STABILITY = SECS_5_MINS
    LL_HIGHER_EDGE_STABILITY = SECS_15_MINS
    NORMAL_EDGE_STABILITY_POINTS = 75
    HIGH_EDGE_STABILITY_POINTS = 125
    LL_NORMAL_QUEUE_LEN = 50
    LL_HIGHER_QUEUE_LEN = 100
    NORMAL_QUEUE_LEN_POINTS = 100
    HIGH_QUEUE_LEN_POINTS = 200
    DECONGESTION_DELAY_PUNISHMENT = -0.5
    FREE_FLOWING_DELAY_PUNISHMENT = -0.5
    AVG_MONTHLY_EDGES_PUNISHMENT = -10
    PRECEDING_CONGESTION_DIVISOR = 100
elif strength == 2:
    MIN_INCIDENT_POINTS = 350
    DELAY_DROP_SCALAR = 1
    MIN_EDGE_STABILITY = SECS_15_MINS
    LL_HIGHER_EDGE_STABILITY = SECS_30_MINS
    NORMAL_EDGE_STABILITY_POINTS = 50  ## less generous
    HIGH_EDGE_STABILITY_POINTS = 100
    LL_NORMAL_QUEUE_LEN = 100
    LL_HIGHER_QUEUE_LEN = 200
    NORMAL_QUEUE_LEN_POINTS = 50  ## less generous
    HIGH_QUEUE_LEN_POINTS = 100
    DECONGESTION_DELAY_PUNISHMENT = -2  ## more punishment ;-)
    FREE_FLOWING_DELAY_PUNISHMENT = -2
    AVG_MONTHLY_EDGES_PUNISHMENT = -20
    PRECEDING_CONGESTION_DIVISOR = 50  ## giving more weight by being smaller (is a denominator)

