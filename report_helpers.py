#! /usr/bin/env python3

from pprint import pprint as pp

import conf
import seg_helpers

HEADING2ARROW = {
    'S': "\N{DOWNWARDS BLACK ARROW}",
    'N': "\N{UPWARDS BLACK ARROW}",
    'NE': "\N{NORTH EAST BLACK ARROW}",
    'NW': "\N{NORTH WEST BLACK ARROW}",
    'SE': "\N{SOUTH EAST BLACK ARROW}",
    'SW': "\N{SOUTH WEST BLACK ARROW}",
    'W': "\N{LEFTWARDS BLACK ARROW}",
    'E': "\N{BLACK RIGHTWARDS ARROW}",
}

def reverse2color(reverse):
    color = 'red' if not reverse else 'green'
    return color

def get_report_id(uuid, inc_dt):
    report_id = "{}_{}".format(uuid, inc_dt.strftime("%Y-%m-%dT%H:%M:%S"))
    return report_id

def _get_ybounds(cur, uuids, dates_clause):
    uuids_clause = seg_helpers.uuids2clause(uuids)
    sql = """\
    SELECT
      MIN(adjusted_delay_pct) AS
    ymin,
      MAX(adjusted_delay_pct) AS
    ymax
    FROM {tbl}
    WHERE {uuids_clause}
    AND {dates_clause}
    """.format(tbl=conf.TRAFFIC_FINAL_TBL, uuids_clause=uuids_clause,
        dates_clause=dates_clause)
    cur.execute(sql)
    ymin, ymax = cur.fetchone()
    return ymin, ymax


def _get_pre_inc_start_idx(dates2sample, inc_start):
    """
    Find highest index with a date before (or on) the start date we're
    interested in.
    """
    debug = False
    n_dates = len(dates2sample)
    inc_start_idx = n_dates - 1
    if debug:
        for i, date2sample in enumerate(dates2sample):
            print(i, date2sample)
    while True:
        if dates2sample[inc_start_idx] <= inc_start:
            break
        else:
            inc_start_idx -= 1
        if inc_start_idx < 0:
            break
    return inc_start_idx

def get_snapshot_dates(cur, focus_dt, dates_clause, uuids, uuids_clause,
        date_offsets, debug=False):
    """
    Only interested in snapshots around same-direction travel (i.e. not
    reversed).
    """
    n_segs = len(uuids)
    sql_get_dates = """\
    SELECT
        date,
        freq
        FROM (
            SELECT
            date,
              COUNT(*) AS
            freq
            FROM {traffic_incidents}
            WHERE {uuids_clause}
            AND {dates_clause}
            GROUP BY date
        ) AS qry
        WHERE freq = %s  /* insisting that there must be data for all segments in run or will ignore */
        ORDER BY date;
    """.format(traffic_incidents=conf.TRAFFIC_FINAL_TBL,
        uuids_clause=uuids_clause, dates_clause=dates_clause)
    cur.execute(sql_get_dates, (n_segs, ))
    dates2sample = [x['date'] for x in cur.fetchall()]
    if not dates2sample:
        raise Exception("Unable to find snapshot dates with {} segments "
            "using\n{}".format(sql_get_dates, n_segs))
    try:
        focus_dt_idx = dates2sample.index(focus_dt)
    except ValueError:
        focus_dt_idx = _get_pre_inc_start_idx(dates2sample, focus_dt)
    ## more focus close to middle and more after the incident supposedly starts
    if debug:
        print(focus_dt)
        print(focus_dt_idx)
        pp(dates2sample)
    dates2use = []
    for date_offset in date_offsets:
        idx = focus_dt_idx + date_offset
        try:
            dates2use.append(dates2sample[idx])
        except IndexError:
            continue
    return dates2use

def get_snapshot_data(cur, date, uuids_clause):
    sql_date_dets = """\
    SELECT
    uuid,
    src_level,
    heading,
      sequence_id AS
    seq,
      adjusted_delay_pct AS
    increase
    FROM
    {traffic_incidents}
    INNER JOIN
    {segdets}
    USING(uuid)
    WHERE {uuids_clause}
    AND date = %s
    ORDER BY seq
    """.format(traffic_incidents=conf.TRAFFIC_FINAL_TBL,
        segdets=conf.SEGDETS_TBL, uuids_clause=uuids_clause)
    cur.execute(sql_date_dets, (date, ))
    data = cur.fetchall()
    return data
